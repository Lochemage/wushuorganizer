Licensing:
----------

Copyright (C) 2013 Jeff P. Houde (Lochemage)

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 3 of the License, or (at your option)
any later version.

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA


Introduction:
-------------

Welcome to the Wushu Organizer Readme!

	The Wushu Organizer is a helper application to organize items within an MMORPG
called Age of Wushu, by Snail Games. The organizer is broken down into two parts,
a gear distribution organizer and a sale organizer.

Gear Distribution Organizer:

	Age of Wushu is a very heavily player based economy. Any quality armor, jewelry,
or weaponry will be crafted by a player rather than looted from an enemy. Because
of this, a guild (at least a good one) plays a heavy role on item crafting and
distribution within its members. This organizer is designed to keep track of gear
needed for all its members. It also has a built in feature to export table information
to an HTML or JavaScript format for easily porting the data to a website.

Sales Management Organizer:

	Because this game has a heavily player based economy, a lot of the game is spent
buying and selling between players. This organizer is designed to keep track of item
prices, as well as other players who provide/need those items. This particular
organizer can be useful for each member on an individual basis, as item sales can be
useful for anyone. 

View the latest tutorial here:

	https://github.com/Lochemage/WushuOrganizer/wiki/Tutorial


Building from Source:
---------------------
	Download the source here: http://lochemage.github.com/WushuOrganizer

	WushuOrganizer uses CMake (http://www.cmake.org) to generate the project files
and can be generated for multiple IDE's and platform types.

	Once the necessary dependancies (see below) have been installed, use CMake to
configure the project for your platform. I recommend building the binaries in a sub
folder such as "build", this will keep all generated files from mixing with the
primary source files.


Dependancies:
-------------
 - Qt Libraries of version 4.7.4 or later (http://qt.nokia.com/downloads/downloads#qt-lib)
      Note: As of Qt 5.0, header file locations and library names have been changed.  If
      you wish to use it, you will have to modify folder paths and includes within the
      source to match up.
