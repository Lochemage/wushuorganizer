/*
 * WushuOrganizer
 * Copyright (C) 2013 Jeff P. Houde (Lochemage)
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef CLICKABLE_LABEL_H
#define CLICKABLE_LABEL_H

#include <Constants.h>

#include <QLabel>
#include <QMouseEvent>


/**
 * A simple overloaded label widget that reacts on a double click
 * with the left mouse button.
 */
class ClickableLabel: public QLabel
{
   Q_OBJECT
public:
   ClickableLabel(QWidget* parent = 0)
      : QLabel(parent)
      , mMouseDown(false)
   {
   }

   ~ClickableLabel()
   {
   }

signals:
   void singleClicked();
   void doubleClicked();

protected:

   void mousePressEvent(QMouseEvent *event)
   {
      if (event->button() == Qt::LeftButton)
      {
         mMouseDown = true;
      }
      QLabel::mousePressEvent(event);
   }

   void mouseReleaseEvent(QMouseEvent *event)
   {
      if (mMouseDown)
      {
         mMouseDown = false;

         emit singleClicked();
      }
      QLabel::mouseReleaseEvent(event);
   }

   void mouseDoubleClickEvent(QMouseEvent* event)
   {
      if (event->button() == Qt::LeftButton)
      {
         emit doubleClicked();
      }
      QLabel::mouseDoubleClickEvent(event);
   }

private:

   bool mMouseDown;
};


#endif // CLICKABLE_LABEL_H
