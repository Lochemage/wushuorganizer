/*
 * WushuOrganizer
 * Copyright (C) 2013 Jeff P. Houde (Lochemage)
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef COLORED_COMBO_BOX_H
#define COLORED_COMBO_BOX_H

#include <Constants.h>
#include <NoScrollComboBox.h>
#include <QComboBox>
#include <QWheelEvent>

class ColoredComboBox: public NoScrollComboBox
{
   Q_OBJECT;

public:
   ColoredComboBox(QWidget *widget = 0)
      : NoScrollComboBox(widget)
   {
   }

   virtual ~ColoredComboBox()
   {
   }

   void AddItems(const QStringList& textList, const QList<QColor>& colorList)
   {
      int startIndex = count();
      addItems(textList);

      for (int index = 0; index < textList.count(); ++index)
      {
         if (index >= colorList.count())
         {
            return;
         }

         const QColor& color = colorList[index];
         SetItemColor(startIndex + index, color);
      }
   }

   bool SetItemColor(int index, const QColor& color)
   {
      if (index > -1 && index < count())
      {
         const QModelIndex idx = model()->index(index, 0);
         model()->setData(idx, color, Qt::BackgroundColorRole);
         return true;
      }

      return false;
   }

private:
};

#endif // COLORED_COMBO_BOX_H
