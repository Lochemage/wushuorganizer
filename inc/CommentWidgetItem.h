/*
 * WushuOrganizer
 * Copyright (C) 2013 Jeff P. Houde (Lochemage)
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef COMMENT_WIDGET_ITEM_H
#define COMMENT_WIDGET_ITEM_H

#include <Constants.h>
#include <ClickableLabel.h>

#include <QtCore/QSettings>

#include <QTableWidgetItem>
#include <QToolButton>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QTextEdit>
#include <QStackedWidget>
#include <QMouseEvent>
#include <QDialog>
#include <QPushButton>

/**
 * A simple popup dialog box to edit comments.
 */
class CommentDialog: public QDialog
{
   Q_OBJECT;
public:
   CommentDialog(QWidget* parent = 0)
      : QDialog(parent)
   {
      setWindowTitle("Comment Edit");
      setModal(true);
      setFocusPolicy(Qt::StrongFocus);
      setFocus();

      QGridLayout* mainLayout = new QGridLayout();

      mTitle = new QLabel();
      mTitle->setAlignment(Qt::AlignCenter);
      mainLayout->addWidget(mTitle, 0, 0, 1, 2);

      QLabel* titleLabel = new QLabel("Comment:");
      titleLabel->setAlignment(Qt::AlignRight);
      mainLayout->addWidget(titleLabel, 1, 0, 1, 1);

      QLabel* commentLabel = new QLabel("Detailed: ");
      commentLabel->setAlignment(Qt::AlignRight);
      mainLayout->addWidget(commentLabel, 2, 0, 1, 1);

      mCommentEdit = new QLineEdit();
      mainLayout->addWidget(mCommentEdit, 1, 1, 1, 1);

      mDetailEdit = new QTextEdit();
      mainLayout->addWidget(mDetailEdit, 2, 1, 1, 1);

      QHBoxLayout* buttonLayout = new QHBoxLayout();
      mainLayout->addLayout(buttonLayout, 3, 1, 1, 1);

      QPushButton* acceptButton = new QPushButton("Accept");
      QPushButton* cancelButton = new QPushButton("Cancel");
      buttonLayout->addWidget(acceptButton);
      buttonLayout->addWidget(cancelButton);

      setLayout(mainLayout);

      connect(acceptButton, SIGNAL(clicked(bool)), this, SLOT(OnAccept(bool)));
      connect(cancelButton, SIGNAL(clicked(bool)), this, SLOT(OnReject(bool)));
   }

   ~CommentDialog()
   {
   }

   void SetTitle(const QString& text)
   {
      mTitle->setText(text);
   }

   QString GetComment() const
   {
      return mCommentEdit->text();
   }

   void SetComment(const QString& text)
   {
      mCommentEdit->setText(text);
   }

   QString GetDetail() const
   {
      return mDetailEdit->toPlainText();
   }

   void SetDetail(const QString& text)
   {
      mDetailEdit->setText(text);
   }

public slots:

   void OnAccept(bool)
   {
      accept();
   }

   void OnReject(bool)
   {
      reject();
   }

private:

   QLabel*    mTitle;
   QLineEdit* mCommentEdit;
   QTextEdit* mDetailEdit;
};

class CommentWidgetItem: public QWidget, public QTableWidgetItem
{
   Q_OBJECT;
public:

   enum StackPage
   {
      LABEL_STACK = 0,
      EDIT_STACK,
   };

   CommentWidgetItem()
      : mNameWidget(NULL)
      , mGuildWidget(NULL)
   {
      mEditButton = new QToolButton();
      mEditButton->setText("...");
      
      mCommentLabel = new ClickableLabel();

      mCommentEdit = new QLineEdit();

      mCommentStack = new QStackedWidget();
      mCommentStack->addWidget(mCommentLabel);
      mCommentStack->addWidget(mCommentEdit);
      mCommentStack->setCurrentIndex(LABEL_STACK);

      QHBoxLayout* layout = new QHBoxLayout();
      layout->setMargin(0);
      layout->setSpacing(2);
      layout->addWidget(mEditButton);
      layout->addWidget(mCommentStack);
      layout->setStretch(0, 0);
      layout->setStretch(1, 1);
      setLayout(layout);

      this->setContentsMargins(0, 0, 0, 0);

      connect(mEditButton, SIGNAL(clicked(bool)), this, SLOT(OnEditButton(bool)));
      connect(mCommentLabel, SIGNAL(doubleClicked()), this, SLOT(OnEditStarted()));
      connect(mCommentEdit, SIGNAL(editingFinished()), this, SLOT(OnEditFinished()));
   }

   bool operator < (const QTableWidgetItem& other) const
   {
      const CommentWidgetItem& item = dynamic_cast<const CommentWidgetItem&>(other);
      return (mCommentLabel->text() < item.mCommentLabel->text());
   }

   void SetNameWidget(QTableWidgetItem* widget)
   {
      mNameWidget = widget;
   }

   void SetGuildWidget(QTableWidgetItem* widget)
   {
      mGuildWidget = widget;
   }

   QString GetComment() const
   {
      return mCommentText;
   }

   void SetComment(const QString& text)
   {
      if (text != mCommentText)
      {
         mCommentText = text;
         mCommentLabel->setText(text);
         mCommentEdit->setText(text);
         emit edited(1);
      }
   }

   QString GetDetail() const
   {
      return mDetailText;
   }

   void SetDetail(const QString& comment)
   {
      if (mDetailText != comment)
      {
         mDetailText = comment;
         if (mDetailText.isEmpty())
         {
            mEditButton->setText("...");
         }
         else
         {
            mEditButton->setText("!!!");
         }
         
         emit edited(1);
      }
   }

public slots:
   void OnEditButton(bool)
   {
      QString title;
      if (mNameWidget && mGuildWidget)
      {
         title = mNameWidget->text();
         if (!mGuildWidget->text().isEmpty())
         {
            title += " (" + mGuildWidget->text() + ")";
         }
      }
      CommentDialog dlg = CommentDialog(NULL);
      dlg.SetTitle(title);
      dlg.SetComment(GetComment());
      dlg.SetDetail(GetDetail());

      if (dlg.exec() == QDialog::Accepted)
      {
         SetComment(dlg.GetComment());
         SetDetail(dlg.GetDetail());
      }
   }

   void OnEditStarted()
   {
      mCommentStack->setCurrentIndex(EDIT_STACK);
      mCommentEdit->setText(mCommentLabel->text());
      mCommentEdit->setFocus(Qt::MouseFocusReason);
      mCommentEdit->selectAll();
   }

   void OnEditFinished()
   {
      mCommentStack->setCurrentIndex(LABEL_STACK);
      
      SetComment(mCommentEdit->text());
   }

signals:
   void edited(int);

private:

   QTableWidgetItem*       mNameWidget;
   QTableWidgetItem*       mGuildWidget;

   QToolButton*            mEditButton;

   QStackedWidget*         mCommentStack;
   ClickableLabel*   mCommentLabel;
   QLineEdit*              mCommentEdit;

   QString                 mCommentText;
   QString                 mDetailText;
};

#endif // COMMENT_WIDGET_ITEM_H
