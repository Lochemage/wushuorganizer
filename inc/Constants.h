/*
 * WushuOrganizer
 * Copyright (C) 2013 Jeff P. Houde (Lochemage)
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <QColor>
#include <QString>
#include <QMap>

#include <vector>


const static QString COMPANY_NAME = "Lochemage";
const static QString APPLICATION_NAME = "WushuOrganizer";

const static int DEFAULT_GRADE = 3;


struct PartData
{
   QString part;
   QString grade;
   QString weaponType;

   PartData()
   {
   }

   PartData(QString search)
   {
      part = search;
   }

   bool operator==(const PartData& other)
   {
      return other.part == part;
   }
};

static QStringList MakeList(const QString& str)
{
   return str.split(", ", QString::SkipEmptyParts);
}

static QMap<QString, QStringList> MakeMapList(const QString& str)
{
   QMap<QString, QStringList> result;
   QString key = "Default";
   QStringList fullList = MakeList(str);
   int count = fullList.count();
   for (int index = 0; index < count; ++index)
   {
      QString token = fullList[index];
      if (token.startsWith("<") && token.endsWith(">"))
      {
         token.remove("<");
         token.remove(">");
         key = token;
         result["Key"].push_back(key);
      }
      else
      {
         result[key].push_back(token);
      }
   }
   return result;
}

static QList<QColor> MakeColors()
{
   QList<QColor> list;
   list.push_back(Qt::transparent);
   list.push_back(QColor(Qt::cyan).light(150));
   list.push_back(Qt::green);
   list.push_back(QColor(Qt::yellow).light(100));
   list.push_back(Qt::magenta);
   list.push_back(QColor(Qt::red).light(100));
   return list;
}

static QStringList MakeColorNames()
{
   QStringList list;
   list.push_back("None");
   list.push_back("Blue");
   list.push_back("Green");
   list.push_back("Yellow");
   list.push_back("Pink");
   list.push_back("Red");
   return list;
}

const QList<QColor> COLOR_LIST = MakeColors();
const QStringList COLOR_NAME_LIST = MakeColorNames();

const QStringList TAILOR_LIST =  MakeList("Head, Top, Wrists, Pants, Shins, Boots");
const QStringList JEWELRY_LIST = MakeList("Neck, Ear1, Ear2, Ring1, Ring2");
const QStringList WEAPON_LIST =  MakeList("Weapon1, Weapon2, Weapon3");
const QStringList PART_LIST = TAILOR_LIST + JEWELRY_LIST + WEAPON_LIST;

const QStringList PRIORITY_LIST = MakeList("MIA, Low, Med, High");
const QStringList SCHOOL_LIST = MakeList("Shaolin, Wudang, Emei, Tangmen, Beggar Sect, Scholars, Royal Guard, Wanderers Valley, Unschooled");
const QStringList ATTRIBUTE_LIST = MakeList("Yin, Yang, Tai Chi");
const QStringList STAT_LIST = MakeList("Brawn, Dexterity, Breath, Spirit, Stamina");
const QStringList WEAPON_TYPE_LIST = MakeList("Single Sword, Twin Sword, Single Blade, Twin Blade, Single Dagger, Twin Dagger, Staff, Stick, Claw");
const QStringList GRADE_LIST = MakeList("Wood, Iron, Copper, Silver, Gold, Jade");
//const QStringList OLD_ITEM_TYPE_LIST = MakeList("Material, Consumable, Formula, Martial Skill, Flying Skill, Fragment, Armor, Jewelry, Weapon, Mask");
const QMap<QString, QStringList> ITEM_TYPE_LIST = MakeMapList(
   "<Consumable>, Food, Health Pill, Energy Pill, Drug, Poison, Training Scroll, "\
   "<Material>, Crafting, Upgrade, "\
   "<Formula>, Tailor, Cook, Poison Maker, Craftsman, Blacksmith, Herbalist, Musician, Weiqi Player, Calligrapher, Painter, Divinator, Beggar, "\
   "<Crafting Table>, Tailor, Cook, Poison Maker, Craftsman, Blacksmith, Herbalist, "\
   "<Skill>, Martial Arts, Flying, "\
   "<Fragment>, Martial Arts, Annotation, "\
   "<Armor>, Head, Top, Wrists, Pants, Shins, Boots, Fashion, "\
   "<Jewelry>, Necklace, Ear, Ring, "\
   "<Weapon>, Single Sword, Twin Sword, Single Blade, Twin Blade, Single Dagger, Twin Dagger, Staff, Stick, Claw, Needle, Pellet, Throwing Knife, Dart, "\
   "<Assistant Item>, Skill Illustration Book, Skill Annotation Book, "\
   "<Mask>, Mask");
const QStringList ACTION_LIST = MakeList("Buy from, Buy for, Sell to, Sell for");


const int COLOR_COUNT = COLOR_NAME_LIST.count();
const int PART_COUNT = PART_LIST.count();
const int PRIORITY_COUNT = PRIORITY_LIST.count();
const int SCHOOL_COUNT = SCHOOL_LIST.count();
const int ATTRIBUTE_COUNT = ATTRIBUTE_LIST.count();
const int STAT_COUNT = STAT_LIST.count();
const int WEAPON_TYPE_COUNT = WEAPON_TYPE_LIST.count();
const int GRADE_COUNT = GRADE_LIST.count();
const int ITEM_TYPE_COUNT = ITEM_TYPE_LIST.count();

enum LoadErrorType
{
   LOAD_ERROR_NONE,
   LOAD_ERROR_CANCELLED,
   LOAD_ERROR_FAILED,
};

enum TabPageType
{
   TAB_PAGE_GEAR = 0,
   TAB_PAGE_SALES,
};

enum GearColumnType
{
   GEAR_COLUMN_COLOR = 0,
   GEAR_COLUMN_NAME,
   GEAR_COLUMN_GUILD,
   GEAR_COLUMN_PRIORITY,
   GEAR_COLUMN_SCHOOL,
   GEAR_COLUMN_ATTRIBUTE,
   GEAR_COLUMN_STATS,

   GEAR_COLUMN_PART_START_INDEX,
};

const int GEAR_COLUMN_DELETE = GEAR_COLUMN_PART_START_INDEX + PART_COUNT;
const int GEAR_COLUMN_HIDE = GEAR_COLUMN_DELETE + 1;
const int GEAR_COLUMN_COMMENT = GEAR_COLUMN_HIDE + 1;

const int WEAPON_START_INDEX = TAILOR_LIST.count() + JEWELRY_LIST.count();


enum SalesColumnType
{
   SALES_COLUMN_NAME = 0,
   SALES_COLUMN_TYPE,
   SALES_COLUMN_SUB_TYPE,
   SALES_COLUMN_GRADE,
   SALES_COLUMN_SELL_PRICE,
   SALES_COLUMN_BUY_PRICE,
   SALES_COLUMN_CUSTOMERS,
   SALES_COLUMN_DELETE,
   SALES_COLUMN_DATA,
};

enum SalesStackType
{
   SALES_STACK_ITEMS = 0,
   SALES_STACK_PEOPLE,
};

enum SalesPersonColumnType
{
   SALES_PERSON_COLUMN_ACTION = 0,
   SALES_PERSON_COLUMN_NAME,
   SALES_PERSON_COLUMN_COUNT,
   SALES_PERSON_COLUMN_PRICE,
   SALES_PERSON_COLUMN_DELETE,
   SALES_PERSON_COLUMN_DATA,
};

static QList<PartData> MakePartList(const QStringList& strList, int defaultGrade = -1)
{
   QList<PartData> result;
   for (int strIndex = 0; strIndex < strList.count(); ++strIndex)
   {
      PartData data;
      data.grade = "None";
      data.weaponType = "No Type";

      QStringList strData = strList[strIndex].split(":", QString::SkipEmptyParts);
      if (strData.count() > 0)
      {
         data.part = strData[0];
      }

      if (strData.count() > 1)
      {
         data.grade = strData[1];
      }
      else if (defaultGrade > -1)
      {
         data.grade = GRADE_LIST[defaultGrade];
      }

      if (strData.count() > 2)
      {
         data.weaponType = strData[2];
      }
      result.push_back(data);
   }
   return result;
}

static QColor GetColorForGrade(int grade)
{
   switch (grade)
   {
   case 0: // Wood
      return QColor(Qt::red).dark(150);
   case 1: // Iron
      return QColor(Qt::cyan).dark(150);
   case 2: // Copper
      return QColor(156, 103, 73);
   case 3: // Silver
      return Qt::gray;
   case 4: // Gold
      return QColor(Qt::yellow).dark(130);
   case 5: // Jade
      return QColor(Qt::green).dark(150);
   default:
      return Qt::white;
   }

   return Qt::white;
}











// Project XML elements
static const char* XML_ORGANIZER_ELEMENT = "Organizer";
static const char* XML_VERSION_ELEMENT = "Version";
static const char* XML_MEMBERS_ELEMENT = "Members";
static const char* XML_MEMBER_ELEMENT = "Member";
static const char* XML_COLOR_ELEMENT = "Color";
static const char* XML_NAME_ELEMENT = "Name";
static const char* XML_GUILD_ELEMENT = "Guild";
static const char* XML_PRIORITY_ELEMENT = "Priority";
static const char* XML_SCHOOL_ELEMENT = "School";
static const char* XML_ATTRIBUTE_ELEMENT = "Attribute";
static const char* XML_STATS_ELEMENT = "Stats";
static const char* XML_STAT_ELEMENT = "Stat";
static const char* XML_PARTS_ELEMENT = "Parts";
static const char* XML_PART_ELEMENT = "Part";
static const char* XML_HIDDEN_ELEMENT = "Hidden";
static const char* XML_COMMENT_ELEMENT = "Comment";
static const char* XML_COMMENT_SHORT_ELEMENT = "Short";
static const char* XML_COMMENT_DETAIL_ELEMENT = "Detail";
static const char* XML_SALES_ELEMENT = "Sales";
static const char* XML_ITEMS_ELEMENT = "Items";
static const char* XML_ITEM_ELEMENT = "Item";
static const char* XML_TYPE_ELEMENT = "Type";
static const char* XML_SUB_TYPE_ELEMENT = "SubType";
static const char* XML_GRADE_ELEMENT = "Grade";
static const char* XML_STACK_SIZE_ELEMENT = "StackSize";
static const char* XML_PRICE_ELEMENT = "Price";
static const char* XML_SELL_PRICE_ELEMENT = "SellPrice";
static const char* XML_BUY_PRICE_ELEMENT = "BuyPrice";
static const char* XML_MIN_ELEMENT = "Min";
static const char* XML_MAX_ELEMENT = "Max";
static const char* XML_SUPPLIERS_ELEMENT = "Suppliers";
static const char* XML_SUPPLIER_ELEMENT = "Supplier";
static const char* XML_CUSTOMERS_ELEMENT = "Customers";
static const char* XML_CUSTOMER_ELEMENT = "Customer";
static const char* XML_ACTION_ELEMENT = "Action";
static const char* XML_COUNT_ELEMENT = "Count";
static const char* XML_FILTERS_ELEMENT = "Filters";
static const char* XML_MIA_ELEMENT = "MIA";
static const char* XML_ARMOR_ELEMENT = "Armor";
static const char* XML_JEWELRY_ELEMENT = "Jewelry";
static const char* XML_WEAPONS_ELEMENT = "Weapons";
static const char* XML_TEXT_ELEMENT = "Text";
static const char* XML_LOADING_COUNT_ELEMENT = "LoadingCount";

// Gear Export elements
static const char* EXPORT_TABLE_ELEMENT = "table";
static const char* EXPORT_FONT_ELEMENT = "font";
static const char* EXPORT_TABLE_ROW_ELEMENT = "tr";
static const char* EXPORT_TABLE_COL_ELEMENT = "td";
static const char* EXPORT_STYLE_ELEMENT = "style";
static const char* EXPORT_WIDTH_ELEMENT = "width";
static const char* EXPORT_CELLSPACING_ELEMENT = "cellspacing";
static const char* EXPORT_CELLPADDING_ELEMENT = "cellpadding";
static const char* EXPORT_COLOR_ELEMENT = "color";
static const char* EXPORT_BACKGROUND_COLOR_ELEMENT = "bgcolor";
static const char* EXPORT_BOLD_ELEMENT = "b";

#endif  // CONSTANTS_H
