/*
 * WushuOrganizer
 * Copyright (C) 2013 Jeff P. Houde (Lochemage)
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef GRADE_COMBO_BOX_H
#define GRADE_COMBO_BOX_H

#include <Constants.h>
#include <ColoredComboBox.h>

const QString STYLE_SHEET = "QComboBox::drop-down {border-width: 0px;} QComboBox::down-arrow {image: url(noimg); border-width: 0px;} QComboBox { background-color: %1; color: %1; }";

class GradeComboBox: public ColoredComboBox
{
   Q_OBJECT;
public:

   GradeComboBox(QWidget* parent = NULL)
      : ColoredComboBox(parent)
   {
      addItem("None");
      SetItemColor(0, GetColorForGrade(-1));
      for (int grade = 0; grade < GRADE_COUNT; ++grade)
      {
         addItem(GRADE_LIST[grade]);
         SetItemColor(grade+1, GetColorForGrade(grade));
      }
      setAutoFillBackground(true);
      setCurrentIndex(0);
      setMinimumWidth(20);
      setMaximumWidth(20);
      setContentsMargins(0, 0, 0, 0);
      view()->setMinimumWidth(60);
      setEditable(true);

      QColor color = GetColorForGrade(-1);
      setStyleSheet(STYLE_SHEET.arg(color.name()));

      connect(this, SIGNAL(currentIndexChanged(int)), this, SLOT(OnStateChanged(int)));
   }

public slots:

   void setCurrentIndex(int index)
   {
      ColoredComboBox::setCurrentIndex(index);
      QColor color = GetColorForGrade(index - 1);
      setStyleSheet(STYLE_SHEET.arg(color.name()));
   }

   void OnStateChanged(int state)
   {
      int index = currentIndex();
      QColor color = GetColorForGrade(index - 1);

      setStyleSheet(STYLE_SHEET.arg(color.name()));
   }
};


#endif // GRADE_COMBO_BOX_H
