/*
 * WushuOrganizer
 * Copyright (C) 2013 Jeff P. Houde (Lochemage)
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef ITEM_DATA_WIDGET_H
#define ITEM_DATA_WIDGET_H

#include <Constants.h>
#include <Money.h>
#include <PersonDataWidget.h>
#include <QTableWidgetItem>


struct ItemData
{
   QString name;
   QString type;
   QString subType;
   QString grade;
   int     stackSize;
   Money   sellMinPrice;
   Money   sellMaxPrice;
   Money   buyMinPrice;
   Money   buyMaxPrice;
   QString comment;

   QList<PersonData*> customers;

   ItemData()
   {
      name = "-----------";
      type = ITEM_TYPE_LIST["Key"][0];
      subType = ITEM_TYPE_LIST[type][0];
      grade = GRADE_LIST[0];
      stackSize = 1;
   }

   void ClearCustomers()
   {
      for (int index = 0; index < customers.count(); ++index)
      {
         delete customers[index];
      }
      customers.clear();
   }
};


class ItemDataWidget: public QTableWidgetItem
{
public:

   ItemDataWidget()
   {
   }

   const ItemData& GetData() const
   {
      return mData;
   }

   ItemData& GetData()
   {
      return mData;
   }

   void SetData(const ItemData& data)
   {
      mData = data;
   }

private:

   ItemData mData;
};


#endif // ITEM_DATA_WIDGET_H
