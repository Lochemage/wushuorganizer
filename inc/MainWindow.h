/*
 * WushuOrganizer
 * Copyright (C) 2013 Jeff P. Houde (Lochemage)
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef WINDOW_H
#define WINDOW_H

#include <Constants.h>
#include <QWidget>
#include <QMainWindow>
#include <QProgressDialog>
#include <pugixml.hpp>
#include <ui_MainWindow.h>


const float VERSION = 3.1f;
const int DATA_VERSION = 4;

struct ItemData;
struct PersonData;

class MainWindow : public QMainWindow
{
   Q_OBJECT

public:
   MainWindow();
   ~MainWindow();

public slots:


   // Main
   void on_actionNew_triggered();
   void on_actionOpen_triggered();
   void on_actionSave_triggered();
   void on_actionSave_As_triggered();
   void on_actionQuit_triggered();

   void on_mainTabWidget_currentChanged(int index);


   // Gear
   void on_gearExportButton_clicked(bool);
   void on_gearAddMemberButton_clicked(bool);
   void on_gearRemoveMemberButton_clicked(bool);
   void on_gearHideMemberButton_clicked(bool);

   void on_gearMemberTable_cellChanged(int row, int col);
   void on_gearMemberRowColor_cellChanged(int index);
   void on_gearMemberPriority_cellChanged(int index);
   void on_gearMemberSchool_cellChanged(int index);
   void on_gearMemberAttribute_cellChanged(int index);
   void on_gearMemberStat_valueChanged();
   void on_gearMemberPart_cellChanged(int index);
   void on_gearMemberComment_cellChanged(int index);

   void on_gearFilterShowMIACheck_stateChanged(int);
   void on_gearFilterShowArmorCheck_stateChanged(int);
   void on_gearFilterShowJewelryCheck_stateChanged(int);
   void on_gearFilterShowWeaponsCheck_stateChanged(int);

   void on_gearFilterSearchEdit_editingFinished();
   void on_gearFilterPriorityCombo_currentIndexChanged(int);
   void on_gearFilterSchoolCombo_currentIndexChanged(int);
   void on_gearFilterAttributeCombo_currentIndexChanged(int);
   void on_gearFilterStatCombo_currentIndexChanged(int);
   void on_gearFilterClearButton_clicked(bool);

   void on_gearTableScroll_timeout();


   // Sales
   void on_salesShowPeopleButton_clicked(bool);
   void on_salesShowItemsButton_clicked(bool);
   void on_salesItemAddButton_clicked(bool);

   void on_salesItemTable_itemSelectionChanged();
   void on_salesItemTable_cellChanged(int row, int col);
   void on_salesItemTable_typeComboChanged(int);
   void on_salesItemTable_subTypeComboChanged(int);
   void on_salesItemTable_gradeComboChanged(int);
   void on_salesItemTable_deleteButtonClicked(bool);
   void on_salesItemNameEdit_editingFinished();
   void on_salesItemTypeCombo_currentIndexChanged(int);
   void on_salesItemSubTypeCombo_currentIndexChanged(int);
   void on_salesItemGradeCombo_currentIndexChanged(int);
   void on_salesItemStackSizeSpin_valueChanged(int);
   void on_salesItemMinSellPrice_valueChanged();
   void on_salesItemMaxSellPrice_valueChanged();
   void on_salesItemMinBuyPrice_valueChanged();
   void on_salesItemMaxBuyPrice_valueChanged();
   void on_salesItemCommentEdit_textChanged();

   void on_salesItemFilterSearchEdit_editingFinished();
   void on_salesItemFilterTypeCombo_currentIndexChanged(int);
   void on_salesItemFilterSubTypeCombo_currentIndexChanged(int);
   void on_salesItemFilterGradeCombo_currentIndexChanged(int);
   void on_salesItemFilterClearButton_clicked(bool);

   void on_salesAddCustomerButton_clicked(bool);

   void on_salesItemCustomersTable_cellChanged(int row, int col);
   void on_salesItemCustomersTable_actionIndexChanged(int);
   void on_salesItemCustomersTable_countValueChanged(int);
   void on_salesItemCustomersTable_priceValueChanged();
   void on_salesItemCustomersTable_deleteButtonClicked(bool);

   void on_salesItemTableScroll_timeout();
   void on_salesItemCustomersTableScroll_timeout();


protected:

   void showEvent(QShowEvent* event);
   void closeEvent(QCloseEvent* event);
   void resizeEvent(QResizeEvent* event);
   void keyPressEvent(QKeyEvent* event);


private:

   // Main
   void ClearData();
   int  LoadData(const QString& filename);
   bool SaveData(const QString& filename);

   void SetModified(bool modified);
   bool IsModified() const;
   bool SaveModifiedCheck();
   bool PromptSave(bool forcePrompt = false);

   void StoreWindowState();
   void RestoreWindowState();


   // Gear
   void GearClearData();
   int  GearGetLoadingCount() const;
   int  GearLoadData(pugi::xml_node mainNode, int version, QProgressDialog* progress);
   bool GearSaveData(pugi::xml_node mainNode) const;

   void SetupUIGear();
   bool GearExport(const QString& filename);
   int  GearAddMemberRow(const QString& color, const QString& name, const QString& guild, const QString& priority, const QString& school, const QString& attribute, const QStringList& stats = QStringList(), const QList<PartData>& parts = QList<PartData>(), const QString& comment = "", const QString& detail = "");
   void GearRefresh(bool applyFilters = true);
   void GearRememberTableScroll();
   void GearRestoreTableScroll();
   void GearResetTableScroll(bool scrollToBottom = true);


   // Sales
   void SalesClearData();
   int  SalesGetLoadingCount() const;
   int  SalesLoadData(pugi::xml_node mainNode, int version, QProgressDialog* progress);
   bool SalesSaveData(pugi::xml_node mainNode) const;

   void SetupUISales();
   int  SalesAddItemRow(const ItemData& data);
   void SalesRefreshItems(bool applyFilters = true);
   void SalesRememberItemTableScroll();
   void SalesRestoreItemTableScroll();
   void SalesResetItemTableScroll(bool scrollToBottom = true);

   int  SalesAddCustomerRow(PersonData* personData, bool addNew = true);

   void SalesRememberItemCustomersTableScroll();
   void SalesRestoreItemCustomersTableScroll();
   void SalesResetItemCustomersTableScroll(bool scrollToBottom = true);

   // Main
   Ui_MainWindow  mUI;
   QString        mFileName;
   bool           mModified;
   bool           mShownSales;


   // Gear
   int            mGearTableHScroll;
   int            mGearTableVScroll;


   // Sales
   int            mSalesItemTableHScroll;
   int            mSalesItemTableVScroll;
   int            mSalesPeopleTableHScroll;
   int            mSalesPeopleTableVScroll;
   int            mSalesItemCustomersTableHScroll;
   int            mSalesItemCustomersTableVScroll;
};

#endif // WINDOW_H
