/*
 * WushuOrganizer
 * Copyright (C) 2013 Jeff P. Houde (Lochemage)
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef MONEY_H
#define MONEY_H

class Money
{
public:

   Money()
   {
      mValue = 0L;
   }

   Money(const long long& value)
   {
      mValue = value;
      Clamp();
   }

   operator long long()
   {
      return mValue;
   }

   Money& operator=(const int& other)
   {
      mValue = long long(other);
      Clamp();
      return *this;
   }

   Money& operator=(const long long& other)
   {
      mValue = other;
      Clamp();
      return *this;
   }

   Money& operator-=(const long long& other)
   {
      mValue -= other;
      Clamp();
      return *this;
   }

   Money& operator+=(const long long& other)
   {
      mValue += other;
      Clamp();
      return *this;
   }

   bool operator==(long long other) const
   {
      return mValue == other;
   }

   bool operator!=(long long other) const
   {
      return mValue != other;
   }

   bool operator>(long long other) const
   {
      return mValue > other;
   }

   bool operator<(long long other) const
   {
      return mValue < other;
   }

   int GetDings() const
   {
      return int(mValue / 1000000L);
   }

   int GetLiang() const
   {
      return int(mValue - GetDings() * 1000000L) / 1000L;
   }

   int GetWen() const
   {
      return int(mValue - (GetDings() * 1000000L + GetLiang() * 1000L));
   }

   void SetDings(int value)
   {
      mValue += (value - GetDings()) * 1000000L;

      Clamp();
   }

   void SetLiang(int value)
   {
      mValue += (value - GetLiang()) * 1000L;

      Clamp();
   }

   void SetWen(int value)
   {
      mValue += (value - GetWen());

      Clamp();
   }

private:

   void Clamp()
   {
      if (mValue > 1000000000L)
         mValue = 1000000000L;
      if (mValue < 0)
         mValue = 0;
   }

   long long mValue;
};


#endif  // MONEY_H
