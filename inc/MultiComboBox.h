/*
 * WushuOrganizer
 * Copyright (C) 2013 Jeff P. Houde (Lochemage)
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef MULTI_COMBO_BOX_H
#define MULTI_COMBO_BOX_H

#include <Constants.h>
#include <QApplication>
#include <QItemDelegate>
#include <QAbstractItemView>
#include <QComboBox>
#include <QCheckBox>
#include <QTableWidgetItem>
#include <QStylePainter>
#include <QWheelEvent>

// internal private delegate
class MultiComboListDelegate : public QItemDelegate
{
public:

   MultiComboListDelegate(QObject *parent)
      : QItemDelegate(parent)
   {
   }

   void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
   {
      //Get item data
      bool value = index.data(Qt::UserRole).toBool();
      QString text = index.data(Qt::DisplayRole).toString();

      // fill style options with item data
      const QStyle *style = QApplication::style();
      QStyleOptionButton opt;
      opt.state |= value ? QStyle::State_On : QStyle::State_Off;
      opt.state |= QStyle::State_Enabled;
      opt.text = text;
      opt.rect = option.rect;

      // draw item data as CheckBox
      style->drawControl(QStyle::CE_CheckBox,&opt,painter);
   }

   QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem & option, const QModelIndex & index ) const
   {
      // create check box as our editor
      QCheckBox *editor = new QCheckBox(parent);

      return editor;
   }

   void setEditorData(QWidget *editor, const QModelIndex &index) const
   {
      //set editor data
      QCheckBox *myEditor = static_cast<QCheckBox*>(editor);
      myEditor->setText(index.data(Qt::DisplayRole).toString());
      myEditor->setChecked(index.data(Qt::UserRole).toBool());
   }

   void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
   {
      //get the value from the editor (CheckBox)
      QCheckBox *myEditor = static_cast<QCheckBox*>(editor);
      bool value = myEditor->isChecked();

      //set model data
      QMap<int,QVariant> data;
      data.insert(Qt::DisplayRole,myEditor->text());
      data.insert(Qt::UserRole,value);
      model->setItemData(index,data);
   }

   void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index ) const
   {
      editor->setGeometry(option.rect);
   }
};

class MultiComboBox: public QComboBox
{
   Q_OBJECT;

public:
   MultiComboBox(QWidget *widget = 0)
      : QComboBox(widget)
      , mDisplayText("<None>")
   {
      // set delegate items view 
      view()->setItemDelegate(new MultiComboListDelegate(this));
      //view()->setStyleSheet("  padding: 15px; ");
      // Enable editing on items view
      view()->setEditTriggers(QAbstractItemView::CurrentChanged);

      // set "MultiComboBox::eventFilter" as event filter for items view 
      view()->viewport()->installEventFilter(this);

      // it just cool to have it as defualt ;)
      view()->setAlternatingRowColors(true);
   }

   virtual ~MultiComboBox()
   {
   }

   void wheelEvent(QWheelEvent* event)
   {
      event->ignore();
   }

   bool eventFilter(QObject *object, QEvent *event)
   {
      // don't close items view after we release the mouse button
      // by simple eating MouseButtonRelease in viewport of items view
      if(event->type() == QEvent::MouseButtonRelease && object==view()->viewport()) 
      {
         return true;
      }
      return QComboBox::eventFilter(object,event);
   }

   virtual void paintEvent(QPaintEvent *)
   {
      QStylePainter painter(this);
      painter.setPen(palette().color(QPalette::Text));

      // draw the combobox frame, focusrect and selected etc.
      QStyleOptionComboBox opt;
      initStyleOption(&opt);

      UpdateDisplayText();

      opt.currentText = mDisplayText;
      painter.drawComplexControl(QStyle::CC_ComboBox, opt);

      // draw the icon and text
      painter.drawControl(QStyle::CE_ComboBoxLabel, opt);
   }

   void UpdateDisplayText()
   {
      QString oldVal = mDisplayText;
      mDisplayText.clear();
      int itemCount = count();
      for (int itemIndex = 0; itemIndex < itemCount; ++itemIndex)
      {
         bool selected = itemData(itemIndex, Qt::UserRole).toBool();
         if (selected)
         {
            if (!mDisplayText.isEmpty())
            {
               mDisplayText += ", ";
            }
            mDisplayText += itemData(itemIndex, Qt::DisplayRole).toString();
         }
      }
      if (mDisplayText.isEmpty())
      {
         mDisplayText = "<None>";
      }

      if (mDisplayText != oldVal)
      {
         emit valueChanged();
      }
   }

   bool IsIndexSelected(int index)
   {
      if (index > -1 && index < count())
      {
         return itemData(index, Qt::UserRole).toBool();
      }
      return false;
   }

   QStringList GetSelection() const
   {
      if (mDisplayText == "<None>")
      {
         return QStringList();
      }
      return MakeList(mDisplayText);
   }

   void SetSelection(const QStringList& selection)
   {
      int itemCount = count();
      for (int itemIndex = 0; itemIndex < itemCount; ++itemIndex)
      {
         QString itemName = itemData(itemIndex, Qt::DisplayRole).toString();
         bool selected = selection.contains(itemName);
         setItemData(itemIndex, selected, Qt::UserRole);
         UpdateDisplayText();
      }
   }

   QString GetDisplayText() const
   {
      return mDisplayText;
   }

signals:

   void valueChanged();

private:

   QString mDisplayText;
};

class MultiComboSortItem: public QWidget, public QTableWidgetItem
{
   Q_OBJECT;
public:

   MultiComboSortItem(MultiComboBox* comboItem)
      : mComboItem(comboItem)
   {
   }

   bool operator < (const QTableWidgetItem &other) const
   {
      const MultiComboSortItem& item = dynamic_cast<const MultiComboSortItem&>(other);
      if (mComboItem->GetDisplayText() < item.mComboItem->GetDisplayText())
      {
         return true;
      }
      return false;
   }

private:

   MultiComboBox* mComboItem;
};

#endif // MULTI_COMBO_BOX_H
