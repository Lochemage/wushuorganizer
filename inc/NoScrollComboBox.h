/*
 * WushuOrganizer
 * Copyright (C) 2013 Jeff P. Houde (Lochemage)
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef NO_SCROLL_COMBO_BOX_H
#define NO_SCROLL_COMBO_BOX_H

#include <Constants.h>
#include <QComboBox>
#include <QTableWidgetItem>
#include <QWheelEvent>


class NoScrollComboBox: public QComboBox
{
   Q_OBJECT;

public:
   NoScrollComboBox(QWidget *widget = 0)
      : QComboBox(widget)
   {
   }

   virtual ~NoScrollComboBox()
   {
   }

   void wheelEvent(QWheelEvent* event)
   {
      event->ignore();
   }

private:
};


// Sorting widgets.
class ComboSortItem: public QWidget, public QTableWidgetItem
{
   Q_OBJECT;
public:

   ComboSortItem(QComboBox* comboItem)
      : mComboItem(comboItem)
   {
   }

   bool operator < (const QTableWidgetItem &other) const
   {
      const ComboSortItem& item = dynamic_cast<const ComboSortItem&>(other);
      if (mComboItem->currentIndex() < item.mComboItem->currentIndex())
      {
         return true;
      }
      return false;
   }

private:

   QComboBox* mComboItem;
};


#endif // NO_SCROLL_COMBO_BOX_H
