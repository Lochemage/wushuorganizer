/*
 * WushuOrganizer
 * Copyright (C) 2013 Jeff P. Houde (Lochemage)
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef PART_WIDGET_ITEM_H
#define PART_WIDGET_ITEM_H

#include <Constants.h>
#include <QAbstractItemView>
#include <QSpacerItem>
#include <QHBoxLayout>
#include <GradeComboBox.h>
#include <NoScrollComboBox.h>

class PartWidgetItem: public QWidget, public QTableWidgetItem
{
   Q_OBJECT;
public:

   PartWidgetItem(bool isWeapon = false)
      : mWeaponComboBox(NULL)
      , mGradeComboBox(NULL)
   {
      if (isWeapon)
      {
         mWeaponComboBox = new NoScrollComboBox();
         mWeaponComboBox->addItem("No Type");
         mWeaponComboBox->addItems(WEAPON_TYPE_LIST);
         mWeaponComboBox->setCurrentIndex(0);
         mWeaponComboBox->view()->setMinimumWidth(100);
      }

      mGradeComboBox = new GradeComboBox();

      QHBoxLayout* layout = new QHBoxLayout();
      layout->setMargin(0);
      layout->setSpacing(2);
      layout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding));
      if (mWeaponComboBox)
      {
         layout->addWidget(mWeaponComboBox);
      }
      layout->addWidget(mGradeComboBox);
      layout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding));

      setLayout(layout);

      this->setContentsMargins(0, 0, 0, 0);

      connect(mWeaponComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(OnStateChanged(int)));
      connect(mGradeComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(OnStateChanged(int)));
   }

   bool HasWeapon() const
   {
      return mWeaponComboBox? true: false;
   }

   int GetWeaponType() const 
   {
      if (mWeaponComboBox)
      {
         return mWeaponComboBox->currentIndex();
      }
      return 0;
   }

   void SetWeaponType(int index)
   {
      if (mWeaponComboBox)
      {
         mWeaponComboBox->setCurrentIndex(index);
      }
   }

   void SetWeaponType(const QString& weapon)
   {
      if (mWeaponComboBox)
      {
         mWeaponComboBox->setCurrentIndex(mWeaponComboBox->findText(weapon));
      }
   }

   int GetGrade() const 
   {
      return mGradeComboBox->currentIndex();
   }

   void SetGrade(int index)
   {
      mGradeComboBox->setCurrentIndex(index);
   }

   void SetGrade(const QString& grade)
   {
      mGradeComboBox->setCurrentIndex(mGradeComboBox->findText(grade));
   }

   QString GetValue() const
   {
      QString result;

      int grade = GetGrade() - 1;
      if (grade > -1 && grade < GRADE_COUNT)
      {
         result += GRADE_LIST[grade];
      }

      result += ":";

      int weapon = GetWeaponType() - 1;
      if (weapon > -1 && weapon < WEAPON_TYPE_COUNT)
      {
         result += WEAPON_TYPE_LIST[weapon];
      }

      return result;
   }

   bool operator < (const QTableWidgetItem& other) const
   {
      const PartWidgetItem& item = dynamic_cast<const PartWidgetItem&>(other);
      // Top sort priority for item grade.
      if (mGradeComboBox->currentIndex() < item.mGradeComboBox->currentIndex())
      {
         return true;
      }

      // Secondary sort priority for weapon type if able.
      if (mWeaponComboBox)
      {
         if (mWeaponComboBox->currentIndex() < item.mWeaponComboBox->currentIndex())
         {
            return true;
         }
      }

      return false;
   }

   void wheelEvent(QWheelEvent* event)
   {
      event->ignore();
   }

public slots:
   void OnStateChanged(int state)
   {
      emit stateChanged(state);
   }

signals:
   void stateChanged(int);

private:

   NoScrollComboBox* mWeaponComboBox;
   GradeComboBox* mGradeComboBox;
};

#endif // PART_WIDGET_ITEM_H
