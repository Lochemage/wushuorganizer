/*
 * WushuOrganizer
 * Copyright (C) 2013 Jeff P. Houde (Lochemage)
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef PERSON_DATA_WIDGET_H
#define PERSON_DATA_WIDGET_H

#include <Constants.h>
#include <Money.h>
#include <QTableWidgetItem>


struct PersonData
{
   QString action;
   QString name;
   int     count;
   Money   price;

   PersonData()
      : action(QString(ACTION_LIST[0]))
      , name(QString("-----------"))
      , count(1)
      , price(0L)
   {
   }
};


class PersonDataWidget: public QTableWidgetItem
{
public:

   PersonDataWidget()
   {
   }

   const PersonData* GetData() const
   {
      return mData;
   }

   PersonData* GetData()
   {
      return mData;
   }

   void SetData(PersonData* data)
   {
      mData = data;
   }

private:

   PersonData* mData;
};


#endif // PERSON_DATA_WIDGET_H
