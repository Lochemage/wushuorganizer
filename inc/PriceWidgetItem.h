/*
 * WushuOrganizer
 * Copyright (C) 2013 Jeff P. Houde (Lochemage)
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef PRICE_WIDGET_ITEM_H
#define PRICE_WIDGET_ITEM_H

#include <Constants.h>
#include <Money.h>
#include <ClickableLabel.h>
#include <QTableWidgetItem>
#include <QStackedWidget>
#include <QHBoxLayout>
#include <QSpinBox>
#include <QLabel>

const QStringList PRICE_SUFFIX_LIST = MakeList("Ding, Liang, Wen");
const QStringList PRICE_SHORT_SUFFIX_LIST = MakeList("D, L, W");
const QStringList PRICE_COLOR_LIST  = MakeList("orange, limegreen, black");

class PriceWidgetItem: public QWidget, public QTableWidgetItem
{
   Q_OBJECT;
public:

   enum PriceWidgetType
   {
      PRICE_D,
      PRICE_L,
      PRICE_W,

      PRICE_COUNT,
   };

   enum StackPage
   {
      LABEL_STACK = 0,
      EDIT_STACK,
   };

   PriceWidgetItem(QWidget* parent = NULL, bool useShortLabels = false)
      : QWidget(parent)
      , mUseShortLabels(useShortLabels)
   {
      QHBoxLayout* mainLayout = new QHBoxLayout();
      mainLayout->setSpacing(0);
      mainLayout->setContentsMargins(6, 0, 6, 0);

      QLabel* bracketLabel = new QLabel("[");
      mainLayout->addWidget(bracketLabel);

      for (int index = 0; index < PRICE_COUNT; ++index)
      {
         mSpin[index] = new QSpinBox();
         mSpin[index]->setMinimum(-1);
         mSpin[index]->setMaximum(1000);
         //mSpin[index]->setButtonSymbols(QSpinBox::NoButtons);
         mSpin[index]->setAlignment(Qt::AlignRight);

         mLabel[index] = new ClickableLabel();
         if (useShortLabels)
         {
            mLabel[index]->setText("0" + PRICE_SHORT_SUFFIX_LIST[index]);
         }
         else
         {
            mLabel[index]->setText("0" + PRICE_SUFFIX_LIST[index]);
         }
         mLabel[index]->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
         int height = 30;

         mStack[index] = new QStackedWidget();
         mStack[index]->addWidget(mLabel[index]);
         mStack[index]->addWidget(mSpin[index]);
         mStack[index]->setCurrentIndex(LABEL_STACK);

         mSpin[index]->setFixedHeight(height);
         mLabel[index]->setFixedHeight(height);
         mStack[index]->setFixedHeight(height);

         mainLayout->addWidget(mStack[index]);

         connect(mSpin[index], SIGNAL(valueChanged(int)), this, SLOT(onValueChanged(int)));
         connect(mSpin[index], SIGNAL(editingFinished()), this, SLOT(onEditingFinished()));
         connect(mLabel[index], SIGNAL(singleClicked()), this, SLOT(onLabelClicked()));
      }

      bracketLabel = new QLabel("]");
      mainLayout->addWidget(bracketLabel);

      mainLayout->setAlignment(Qt::AlignCenter);
      setLayout(mainLayout);
   }

   Money GetPrice() const
   {
      Money value;
      value.SetDings(mSpin[PRICE_D]->value());
      value.SetLiang(mSpin[PRICE_L]->value());
      value.SetWen(  mSpin[PRICE_W]->value());
      return value;
   }

   void SetPrice(const Money& value)
   {
      mSpin[PRICE_D]->blockSignals(true);
      mSpin[PRICE_L]->blockSignals(true);
      mSpin[PRICE_W]->blockSignals(true);

      mSpin[PRICE_D]->setValue(value.GetDings());
      mSpin[PRICE_L]->setValue(value.GetLiang());
      mSpin[PRICE_W]->setValue(value.GetWen());

      mSpin[PRICE_D]->blockSignals(false);
      mSpin[PRICE_L]->blockSignals(false);
      mSpin[PRICE_W]->blockSignals(false);

      if (mUseShortLabels)
      {
         for (int index = 0; index < PRICE_COUNT; ++index)
         {
            mLabel[index]->setText("<font color=\'" + PRICE_COLOR_LIST[index] + "\'>" + QString::number(mSpin[index]->value()) + PRICE_SHORT_SUFFIX_LIST[index] + "</font>");
         }
      }
      else
      {
         for (int index = 0; index < PRICE_COUNT; ++index)
         {
            mLabel[index]->setText("<font color=\'" + PRICE_COLOR_LIST[index] + "\'>" + QString::number(mSpin[index]->value()) + PRICE_SUFFIX_LIST[index] + "</font>");
         }
      }
   }

   ////////////////////////////////////////////////////////////////////////////////
   static QString ToString(const Money& value)
   {
      QString result;
      if (value > 0)
      {
         QString spacer = "";
         result += "[";
         if (value.GetDings() > 0)
         {
            result += "<font color=\'" + PRICE_COLOR_LIST[PRICE_D] + "\'>" + QString::number(value.GetDings()) + "D</font>";
            spacer = " ";
         }
         if (value.GetLiang() > 0)
         {
            result += "<font color=\'" + PRICE_COLOR_LIST[PRICE_L] + "\'>" + spacer + QString::number(value.GetLiang()) + "L</font>";
            spacer = " ";
         }
         if (value.GetWen() > 0)
         {
            result += "<font color=\'" + PRICE_COLOR_LIST[PRICE_W] + "\'>" + spacer + QString::number(value.GetWen()) + "W</font>";
         }
         result += "]";
      }
      else
      {
         result += "<font color=\'" + PRICE_COLOR_LIST[PRICE_W] + "\'>[0W]</font>";
      }
      return result;
   }

   bool operator < (const QTableWidgetItem &other) const
   {
      const PriceWidgetItem& item = dynamic_cast<const PriceWidgetItem&>(other);
      if (GetPrice() < item.GetPrice())
      {
         return true;
      }
      return false;
   }

public slots:
   void onLabelClicked()
   {
      for (int index = 0; index < PRICE_COUNT; ++index)
      {
         if (mLabel[index] == sender())
         {
            mStack[index]->setCurrentIndex(EDIT_STACK);
            mSpin[index]->setFocus();
            mSpin[index]->selectAll();
            return;
         }
      }
   }

   void onEditingFinished()
   {
      for (int index = 0; index < PRICE_COUNT; ++index)
      {
         if (mSpin[index] == sender())
         {
            mStack[index]->setCurrentIndex(LABEL_STACK);
            return;
         }
      }
   }

   void onValueChanged(int value)
   {
      SetPrice(GetPrice());

      emit valueChanged();
   }

signals:
   void valueChanged();

private:

   QSpinBox*         mSpin[PRICE_COUNT];
   ClickableLabel*   mLabel[PRICE_COUNT];
   QStackedWidget*   mStack[PRICE_COUNT];

   bool              mUseShortLabels;
};


class MinMaxPriceWidgetItem: public QWidget, public QTableWidgetItem
{
   Q_OBJECT;
public:

   MinMaxPriceWidgetItem(QWidget* parent = NULL, bool useShortLabels = false)
      : QWidget(parent)
   {
      QHBoxLayout* mainLayout = new QHBoxLayout();
      mainLayout->setMargin(0);
      mainLayout->setSpacing(0);

      mPriceMin = new PriceWidgetItem(NULL, useShortLabels);
      mPriceMax = new PriceWidgetItem(NULL, useShortLabels);

      QLabel* label = new QLabel(" - ");
      label->setAlignment(Qt::AlignCenter);

      mainLayout->addWidget(mPriceMin);
      mainLayout->addWidget(label);
      mainLayout->addWidget(mPriceMax);

      connect(mPriceMin, SIGNAL(valueChanged()), this, SIGNAL(valueChanged()));
      connect(mPriceMax, SIGNAL(valueChanged()), this, SIGNAL(valueChanged()));

      mainLayout->setAlignment(Qt::AlignCenter);
      setLayout(mainLayout);
   }

   Money GetMinPrice() const
   {
      return mPriceMin->GetPrice();
   }

   Money GetMaxPrice() const
   {
      return mPriceMax->GetPrice();
   }

   void SetMinPrice(const Money& value)
   {
      mPriceMin->SetPrice(value);
   }

   void SetMaxPrice(const Money& value)
   {
      mPriceMax->SetPrice(value);
   }

   static QString ToString(const Money& minValue, const Money& maxValue)
   {
      QString result;
      if (minValue > 0)
      {
         result += PriceWidgetItem::ToString(minValue);
      }
      if (minValue > 0 && maxValue > 0)
      {
         result += " - ";
      }
      if (maxValue > 0 || (minValue == 0 && maxValue == 0))
      {
         result += PriceWidgetItem::ToString(maxValue);
      }
      return result;
   }

   bool operator < (const QTableWidgetItem &other) const
   {
      const MinMaxPriceWidgetItem& item = dynamic_cast<const MinMaxPriceWidgetItem&>(other);
      if (GetMinPrice() < item.GetMinPrice())
      {
         return true;
      }
      if (GetMaxPrice() < item.GetMaxPrice())
      {
         return true;
      }
      return false;
   }

signals:
      void valueChanged();

private:

   PriceWidgetItem*  mPriceMin;
   PriceWidgetItem*  mPriceMax;
};

#endif // PRICE_WIDGET_ITEM_H
