;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;            Installer script for WushuOrganizer              ;;;;
;;;;                                                             ;;;;
;;;;         To generate the installer, download NSIS            ;;;;
;;;;         (http://nsis.sourceforge.net/Download)              ;;;;
;;;;         and compile using this NSI script.                  ;;;;
;;;;                                                             ;;;;
;;;;     Uses environment variables to find external files       ;;;;
;;;;      QT_DIR - Root Qt SDK folder.                           ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                 Paths to assets and binaries                ;;;;
;;;; - Edit these paths if you have custom locations for files   ;;;;
!define PROJECT_BIN_PATH      "..\..\build\bin"
!define QT_PATH               "$%QT_DIR%\bin"
;;;;                                                             ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                            Setup                            ;;;;
;!define DEBUG_INSTALL
!define VS2008

;!define /date NOW "%m-%d-%Y_%H%M%S"

!ifndef VERSION
  !define VERSION "Version-3.1"
!endif

; HM NIS Edit Wizard helper defines
!define PRODUCT_NAME "WushuOrganizer"
!define PRODUCT_VERSION ${VERSION}
!define PRODUCT_DIR_REGKEY "Software\WushuOrganizer"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"
!define PRODUCT_STARTMENU_REGVAL "NSIS:StartMenuDir"

!ifdef DEBUG_INSTALL
	!define CONFIG_FLAG "d"
	!define CONFIG_FLAG2 "_d"
	!define CONFIG_NAME "Debug"
!else
	!define CONFIG_FLAG ""
	!define CONFIG_FLAG2 ""
	!define CONFIG_NAME ""
!endif

!ifdef OUTFILE
  OutFile "${OUTFILE}"
!else
  OutFile WushuOrganizer${CONFIG_NAME}-${VERSION}-setup.exe
!endif

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                              UI                             ;;;;
; MUI 1.67 compatible ------
!include "MUI.nsh"

; MUI Settings
!define MUI_ABORTWARNING
!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"
!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall.ico"

; Welcome page
!insertmacro MUI_PAGE_WELCOME
; License page
;!insertmacro MUI_PAGE_LICENSE "c:\path\to\licence\YourSoftwareLicence.txt"
; Directory page
!insertmacro MUI_PAGE_DIRECTORY
; Start menu page
var ICONS_GROUP
!define MUI_STARTMENUPAGE_NODISABLE
!define MUI_STARTMENUPAGE_DEFAULTFOLDER "WushuOrganizer"
!define MUI_STARTMENUPAGE_REGISTRY_ROOT "${PRODUCT_UNINST_ROOT_KEY}"
!define MUI_STARTMENUPAGE_REGISTRY_KEY "${PRODUCT_UNINST_KEY}"
!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "${PRODUCT_STARTMENU_REGVAL}"
!insertmacro MUI_PAGE_STARTMENU Application $ICONS_GROUP
; Optional components page
!insertmacro MUI_PAGE_COMPONENTS
; Instfiles page
!insertmacro MUI_PAGE_INSTFILES
; Finish page
!define MUI_FINISHPAGE_RUN "$INSTDIR\WushuOrganizer.exe"
!insertmacro MUI_PAGE_FINISH

; Uninstaller pages
!insertmacro MUI_UNPAGE_INSTFILES

; Language files
!insertmacro MUI_LANGUAGE "English"

; MUI end ------

Name "${PRODUCT_NAME} ${PRODUCT_VERSION}"
InstallDir "C:\WushuOrganizer"
InstallDirRegKey HKLM "${PRODUCT_DIR_REGKEY}" ""
ShowInstDetails show
ShowUnInstDetails show

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                    Application Binaries                     ;;;;
Section "Application" SEC01
  SetOutPath "$INSTDIR"
  SetOverwrite ifnewer

  ; Application binary
  File "${PROJECT_BIN_PATH}\WushuOrganizer${CONFIG_FLAG}.exe"

  ; Documentation.
  File "..\..\LICENSE.txt"
  File "..\..\README.txt"
  File "..\..\Version History.txt"
  
  ; Application dependencies.
  File "${QT_PATH}\QtCore${CONFIG_FLAG}4.dll"
  File "${QT_PATH}\QtGui${CONFIG_FLAG}4.dll"

  ; PDB Files for debug.
!ifdef DEBUG_INSTALL
  SetOutPath "$INSTDIR"
  File "${PROJECT_BIN_PATH}\WushuOrganizer${DEBUG_FLAG}.pdb"
!endif
SectionEnd

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                         Shortcuts                           ;;;;
Section "Shortcuts" SEC02
  ; Start Menu Shortcuts
  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
  SetOutPath "$INSTDIR"
  CreateDirectory "$SMPROGRAMS\$ICONS_GROUP"
  CreateShortCut "$SMPROGRAMS\$ICONS_GROUP\WushuOrganizer.lnk"            "$INSTDIR\WushuOrganizer${CONFIG_FLAG}.exe"
  CreateShortCut "$SMPROGRAMS\$ICONS_GROUP\README.lnk"                    "$INSTDIR\README.txt"
  CreateShortCut "$SMPROGRAMS\$ICONS_GROUP\Uninstall.lnk"                 "$INSTDIR\uninst.exe"
  !insertmacro MUI_STARTMENU_WRITE_END
  
  ; Desktop Shortcuts
  CreateShortCut "$DESKTOP\WushuOrganizer.lnk"                             "$INSTDIR\WushuOrganizer${CONFIG_FLAG}.exe"
SectionEnd

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                        Redistribution                       ;;;;
Section /o "Install VCRedist Package" SEC03
  SetOutPath "$INSTDIR"
!ifdef VS2008
  File "vcredist_x86.exe"
  ExecWait "$INSTDIR\vcredist_x86.exe"
!endif
SectionEnd

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                         Desctiptions                        ;;;;
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
  !insertmacro MUI_DESCRIPTION_TEXT ${SEC01} "The Wushu Organizer application."
  !insertmacro MUI_DESCRIPTION_TEXT ${SEC02} "Install Start Menu shortcuts for the Wushu Organizer application."
  !insertmacro MUI_DESCRIPTION_TEXT ${SEC03} "Install the Visual Studio runtime libraries (install if you don't have Visual Studio already installed)."
!insertmacro MUI_FUNCTION_DESCRIPTION_END

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                          Uninstall                          ;;;;
Section -Post
  WriteUninstaller "$INSTDIR\uninst.exe"
  WriteRegStr HKLM "${PRODUCT_DIR_REGKEY}" "" $INSTDIR
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName" "$(^Name)"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\uninst.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayIcon" "$INSTDIR\bin\ApplicationLoader.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"
SectionEnd

Function un.onUninstSuccess
  HideWindow
  MessageBox MB_ICONINFORMATION|MB_OK "$(^Name) was successfully removed from your computer."
FunctionEnd

Function un.onInit
  MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 "Are you sure you want to completely remove $(^Name) and all of its components?" IDYES +2
  Abort
FunctionEnd

Section Uninstall
  !insertmacro MUI_STARTMENU_GETFOLDER "Application" $ICONS_GROUP
  Delete "$INSTDIR\${PRODUCT_NAME}.url"
  Delete "$INSTDIR\uninst.exe"

  Delete "$INSTDIR\*.*"
  RMDIR /r "$INSTDIR"

  Delete "$SMPROGRAMS\$ICONS_GROUP\*.*"
  RMDIR /r "$SMPROGRAMS\$ICONS_GROUP"

  Delete "$DESKTOP\WushuOrganizer.lnk"

  DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"
  DeleteRegKey HKLM "${PRODUCT_DIR_REGKEY}"
  SetAutoClose true
SectionEnd
