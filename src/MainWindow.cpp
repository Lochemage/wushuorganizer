/*
 * WushuOrganizer
 * Copyright (C) 2013 Jeff P. Houde (Lochemage)
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include <MainWindow.h>

#include <QtGui>
#include <QVariant>
#include <QFile>
#include <QTextStream>
#include <ColoredComboBox.h>
#include <MultiComboBox.h>
#include <NoScrollComboBox.h>
#include <PartWidgetItem.h>
#include <CommentWidgetItem.h>


////////////////////////////////////////////////////////////////////////////////
MainWindow::MainWindow()
   : mFileName("")
   , mModified(false)
   , mShownSales(false)
   , mGearTableHScroll(0)
   , mGearTableVScroll(0)
   , mSalesItemTableHScroll(0)
   , mSalesItemTableVScroll(0)
   , mSalesPeopleTableHScroll(0)
   , mSalesPeopleTableVScroll(0)
{
   mUI.setupUi(this);
   SetupUIGear();
   SetupUISales();
}

////////////////////////////////////////////////////////////////////////////////
MainWindow::~MainWindow()
{
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_actionNew_triggered()
{
   if (SaveModifiedCheck())
   {
      // Clear the table first.
      mUI.gearMemberTable->setRowCount(0);

      mFileName.clear();
      SetModified(false);
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_actionOpen_triggered()
{
   if (SaveModifiedCheck())
   {
      QSettings settings(COMPANY_NAME, APPLICATION_NAME);
      settings.beginGroup("Paths");
      QString oldPath = settings.value("iniFile").toString();
      QString filename = QFileDialog::getOpenFileName(
         this, "Load Data File", oldPath, "Ini (*.ini)");

      if (!filename.isEmpty())
      {
         settings.setValue("iniFile", filename);

         int err = LoadData(filename);
         if (err == LOAD_ERROR_NONE)
         {
            QMessageBox::information(this, "Loaded!", "Data has been loaded.", QMessageBox::Ok);
         }
         else if (err == LOAD_ERROR_FAILED)
         {
            QMessageBox::information(this, "Failed!", "Data failed to load.", QMessageBox::Ok);
         }
      }
      settings.endGroup();
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_actionSave_triggered()
{
   PromptSave(false);
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_actionSave_As_triggered()
{
   PromptSave(true);
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_actionQuit_triggered()
{
   close();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_mainTabWidget_currentChanged(int index)
{
   if (index == TAB_PAGE_SALES)
   {
      mShownSales = true;
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::showEvent(QShowEvent* event)
{
   static bool firstShow = true;

   if (firstShow)
   {
      firstShow = false;

      RestoreWindowState();

      QSettings settings(COMPANY_NAME, APPLICATION_NAME);
      settings.beginGroup("Paths");
      QString oldPath = settings.value("iniFile").toString();
      settings.endGroup();

      if (!oldPath.isEmpty())
      {
         LoadData(oldPath);
      }

      GearRefresh(false);
      SalesRefreshItems(false);
      SetModified(false);
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::closeEvent(QCloseEvent* event)
{
   if (!SaveModifiedCheck())
   {
      event->ignore();
      return;
   }

   StoreWindowState();
   QMainWindow::closeEvent(event);
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::resizeEvent(QResizeEvent* event)
{
   QMainWindow::resizeEvent(event);
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::keyPressEvent(QKeyEvent* event)
{
   if (event->key() == Qt::Key_Escape)
   {
      close();
      return;
   }

   if (event->modifiers() == Qt::ControlModifier)
   {
      if (event->key() == Qt::Key_F)
      {
         int tabIndex = mUI.mainTabWidget->currentIndex();
         if (tabIndex == TAB_PAGE_GEAR)
         {
            mUI.gearFilterSearchEdit->setFocus();
            mUI.gearFilterSearchEdit->selectAll();
         }
         else if (tabIndex == TAB_PAGE_SALES)
         {
            mUI.salesItemFilterSearchEdit->setFocus();
            mUI.salesItemFilterSearchEdit->selectAll();
         }
      }
   }

   QMainWindow::keyPressEvent(event);
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::ClearData()
{
   mFileName.clear();

   GearClearData();
   SalesClearData();
}

////////////////////////////////////////////////////////////////////////////////
int MainWindow::LoadData(const QString& filename)
{
   ClearData();

   pugi::xml_document document;
   document.load_file(filename.toStdString().c_str());

   pugi::xml_node mainNode = document.child(XML_ORGANIZER_ELEMENT);
   if (!mainNode)
   {
      return LOAD_ERROR_FAILED;
   }

   // Check the version.
   int version = mainNode.attribute(XML_VERSION_ELEMENT).as_int();
   if (version > DATA_VERSION)
   {
      return LOAD_ERROR_FAILED;
   }

   int loadCount = mainNode.attribute(XML_LOADING_COUNT_ELEMENT).as_int();
   QProgressDialog progress("Loading Data...", "Cancel", 0, loadCount, this);

   if (loadCount > 0)
   {
      progress.show();
   }

   int err = GearLoadData(mainNode, version, &progress);
   if (err != LOAD_ERROR_NONE)
   {
      ClearData();
      return err;
   }

   err = SalesLoadData(mainNode, version, &progress);
   if (err != LOAD_ERROR_NONE)
   {
      ClearData();
      return err;
   }

   mFileName = filename;
   SetModified(false);
   return LOAD_ERROR_NONE;
}

////////////////////////////////////////////////////////////////////////////////
bool MainWindow::SaveData(const QString& filename)
{
   mFileName = filename;

   pugi::xml_document document;

   pugi::xml_node mainNode = document.append_child(XML_ORGANIZER_ELEMENT);

   // Version
   mainNode.append_attribute(XML_VERSION_ELEMENT).set_value(DATA_VERSION);

   int loadCount = 0;
   loadCount += GearGetLoadingCount();
   loadCount += SalesGetLoadingCount();
   mainNode.append_attribute(XML_LOADING_COUNT_ELEMENT).set_value(QString::number(loadCount).toStdString().c_str());

   if (!GearSaveData(mainNode))
   {
      return false;
   }

   if (!SalesSaveData(mainNode))
   {
      return false;
   }

   document.save_file(filename.toStdString().c_str(), "    ", pugi::format_indent | pugi::format_no_declaration);
   SetModified(false);
   return true;
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::SetModified(bool modified)
{
   mModified = modified;

   QString title = "Wushu Organizer v" + QString::number(VERSION) + " - " + mFileName;

   if (mModified)
   {
      title += "*";
   }

   setWindowTitle(title);
}

////////////////////////////////////////////////////////////////////////////////
bool MainWindow::IsModified() const
{
   return mModified;
}

////////////////////////////////////////////////////////////////////////////////
bool MainWindow::SaveModifiedCheck()
{
   // If there are any changes currently made, prompt the user.
   if (IsModified())
   {
      int response = QMessageBox::question(
         this,
         "Save changes?",
         "There are changes made, do you wish to save these changes first?",
         QMessageBox::Yes,
         QMessageBox::No,
         QMessageBox::Cancel);

      if (response == QMessageBox::Cancel)
      {
         return false;
      }
      else if (response == QMessageBox::Yes)
      {
         if (!PromptSave(false))
         {
            return false;
         }
      }
   }

   return true;
}

////////////////////////////////////////////////////////////////////////////////
bool MainWindow::PromptSave(bool forcePrompt)
{
   QSettings settings(COMPANY_NAME, APPLICATION_NAME);
   settings.beginGroup("Paths");

   QString filename = mFileName;
   if (forcePrompt || mFileName.isEmpty())
   {
      forcePrompt = true;
      QString oldPath = settings.value("iniFile").toString();
      filename = QFileDialog::getSaveFileName(
         this, "Save Data File", oldPath, "Ini (*.ini)");
   }

   if (!filename.isEmpty())
   {
      settings.setValue("iniFile", filename);
      settings.endGroup();

      if (!SaveData(filename))
      {
         QMessageBox::information(this, "Failed!", "Save failed.", QMessageBox::Ok);
         return false;
      }
      if (forcePrompt)
      {
         QMessageBox::information(this, "Saved!", "Data has been saved.", QMessageBox::Ok);
      }
      return true;
   }

   settings.endGroup();
   return false;
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::StoreWindowState()
{
   // Store window geometry.
   QSettings settings(COMPANY_NAME, APPLICATION_NAME);
   // Main
   settings.setValue("geometry", saveGeometry());

   // Gear
   settings.beginGroup("gear");
   {
      settings.beginGroup("windowState");
      {
         QList<int> sizes = mUI.gearMainSplitter->sizes();
         settings.setValue("leftSplitterSize", sizes[0]);
         settings.setValue("rightSplitterSize", sizes[1]);

         // first Un-hide all columns so their sizes are valid.
         for (int partIndex = 0; partIndex < PART_COUNT; ++partIndex)
         {
            mUI.gearMemberTable->setColumnHidden(GEAR_COLUMN_PART_START_INDEX + partIndex, false);
         }

         QList<QVariant> columnSizes;
         for (int columnIndex = 0; columnIndex < mUI.gearMemberTable->columnCount(); ++columnIndex)
         {
            columnSizes.push_back(mUI.gearMemberTable->columnWidth(columnIndex));
         }
         settings.setValue("tableColumnSizes", columnSizes);
      }
      settings.endGroup();
   }
   settings.endGroup();

   // Sales
   if (mShownSales)
   {
      settings.beginGroup("sales");
      {
         settings.beginGroup("windowState");
         {
            QList<int> sizes = mUI.salesMainSplitter->sizes();
            settings.setValue("leftSplitterSize", sizes[0]);
            settings.setValue("rightSplitterSize", sizes[1]);

            sizes = mUI.salesItemSplitter->sizes();
            settings.setValue("leftItemSplitterSize", sizes[0]);
            settings.setValue("rightItemSplitterSize", sizes[1]);

            QList<QVariant> columnSizes;
            for (int columnIndex = 0; columnIndex < mUI.salesItemTable->columnCount(); ++columnIndex)
            {
               columnSizes.push_back(mUI.salesItemTable->columnWidth(columnIndex));
            }
            settings.setValue("itemTableColumnSizes", columnSizes);

            columnSizes.clear();
            for (int columnIndex = 0; columnIndex < mUI.salesItemCustomersTable->columnCount(); ++columnIndex)
            {
               columnSizes.push_back(mUI.salesItemCustomersTable->columnWidth(columnIndex));
            }
            settings.setValue("itemCustomersTableColumnSizes", columnSizes);
         }
         settings.endGroup();
      }
      settings.endGroup();
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::RestoreWindowState()
{
   // Restore window geometry.
   QSettings settings(COMPANY_NAME, APPLICATION_NAME);

   // Bail if we have no settings.
   if (!settings.contains("geometry"))
   {
      return;
   }

   restoreGeometry(settings.value("geometry").toByteArray());

   // Gear
   settings.beginGroup("gear");
   {
      settings.beginGroup("windowState");
      {
         QList<int> sizes;
         sizes.push_back(settings.value("leftSplitterSize", int((width() / 5) * 2)).toInt());
         sizes.push_back(settings.value("rightSplitterSize", int((width() / 5) * 3)).toInt());
         mUI.gearMainSplitter->setSizes(sizes);

         QList<QVariant> columnSizes = settings.value("tableColumnSizes").toList();
         for (int columnIndex = 0; columnIndex < mUI.gearMemberTable->columnCount(); ++columnIndex)
         {
            int w = 25;
            if (columnIndex < columnSizes.count())
            {
               w = columnSizes[columnIndex].toInt();
               if (w <= 25) w = 25;
            }
            mUI.gearMemberTable->setColumnWidth(columnIndex, w);
         }
      }
      settings.endGroup();
   }
   settings.endGroup();

   // Sales
   settings.beginGroup("sales");
   {
      settings.beginGroup("windowState");
      {
         QList<int> sizes;
         sizes.push_back(settings.value("leftSplitterSize", int((width() / 5) * 2)).toInt());
         sizes.push_back(settings.value("rightSplitterSize", int((width() / 5) * 3)).toInt());
         mUI.salesMainSplitter->setSizes(sizes);

         sizes.clear();
         sizes.push_back(settings.value("leftItemSplitterSize", ((width() / 5) * 3) / 2).toInt());
         sizes.push_back(settings.value("rightItemSplitterSize", ((width() / 5) * 3) / 2).toInt());
         mUI.salesItemSplitter->setSizes(sizes);

         QList<QVariant> columnSizes = settings.value("itemTableColumnSizes").toList();
         for (int columnIndex = 0; columnIndex < mUI.salesItemTable->columnCount(); ++columnIndex)
         {
            int w = 25;
            if (columnIndex < columnSizes.count())
            {
               w = columnSizes[columnIndex].toInt();
               if (w < 25) w = 25;
            }
            mUI.salesItemTable->setColumnWidth(columnIndex, w);
         }

         columnSizes = settings.value("itemCustomersTableColumnSizes").toList();
         for (int columnIndex = 0; columnIndex < mUI.salesItemCustomersTable->columnCount(); ++columnIndex)
         {
            int w = 25;
            if (columnIndex < columnSizes.count())
            {
               w = columnSizes[columnIndex].toInt();
               if (w < 25) w = 25;
            }
            mUI.salesItemCustomersTable->setColumnWidth(columnIndex, w);
         }
      }
      settings.endGroup();
   }
   settings.endGroup();
}

////////////////////////////////////////////////////////////////////////////////
