/*
 * WushuOrganizer
 * Copyright (C) 2013 Jeff P. Houde (Lochemage)
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include <MainWindow.h>

#include <QtGui>
#include <QVariant>
#include <QFile>
#include <QTextStream>
#include <ColoredComboBox.h>
#include <MultiComboBox.h>
#include <NoScrollComboBox.h>
#include <PartWidgetItem.h>
#include <CommentWidgetItem.h>


////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_gearExportButton_clicked(bool)
{
   QSettings settings(COMPANY_NAME, APPLICATION_NAME);
   settings.beginGroup("Paths");
   QString oldPath = settings.value("exportFile").toString();
   QString filename = QFileDialog::getSaveFileName(
      this, "GearExport File", oldPath, "Format (*.txt; *.js)");

   if (!filename.isEmpty())
   {
      settings.setValue("exportFile", filename);

      if (!GearExport(filename))
      {
         QMessageBox::information(this, "Failed!", "GearExport failed.", QMessageBox::Ok);
         return;
      }
      QMessageBox::information(this, "Exported!", "Data has been exported.", QMessageBox::Ok);
   }
   settings.endGroup();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_gearAddMemberButton_clicked(bool)
{
   QSettings settings(COMPANY_NAME, APPLICATION_NAME);
   settings.beginGroup("gear");
      settings.beginGroup("defaultMemberTableFields");
         QString defaultColor = COLOR_NAME_LIST[0];
         QString defaultName = "-----------";
         QString defaultGuild = settings.value("guild", "Sanguine Moon").toString();
         QString defaultPriority = settings.value("priority", PRIORITY_LIST[0]).toString();
         QString defaultSchool = settings.value("school", SCHOOL_LIST[0]).toString();
         QString defaultAttribute = settings.value("attribute", ATTRIBUTE_LIST[0]).toString();
      settings.endGroup();
   settings.endGroup();

   // Add the new member to the table.
   GearAddMemberRow(defaultColor, defaultName, defaultGuild, defaultPriority, defaultSchool, defaultAttribute);
   GearRefresh(false);
   GearResetTableScroll();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_gearRemoveMemberButton_clicked(bool)
{
   QPushButton* myItem = dynamic_cast<QPushButton*>(sender());

   int rowCount = mUI.gearMemberTable->rowCount();
   for (int rowIndex = 0; rowIndex < rowCount; ++rowIndex)
   {
      if (mUI.gearMemberTable->cellWidget(rowIndex, GEAR_COLUMN_DELETE) == myItem)
      {
         QTableWidgetItem* nameItem = mUI.gearMemberTable->item(rowIndex, GEAR_COLUMN_NAME);
         if (nameItem)
         {
            if (QMessageBox::question(this,
               "Delete member",
               "Are you sure you wish to delete member: " + nameItem->text() + "?",
               QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes)
            {
               GearRememberTableScroll();
               mUI.gearMemberTable->removeRow(rowIndex);
               SetModified(true);
               GearRefresh(false);
               GearRestoreTableScroll();
            }
         }
         return;
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_gearHideMemberButton_clicked(bool)
{
   QPushButton* myItem = dynamic_cast<QPushButton*>(sender());

   int rowCount = mUI.gearMemberTable->rowCount();
   for (int rowIndex = 0; rowIndex < rowCount; ++rowIndex)
   {
      if (mUI.gearMemberTable->cellWidget(rowIndex, GEAR_COLUMN_HIDE) == myItem)
      {
         GearRememberTableScroll();
         mUI.gearMemberTable->setRowHidden(rowIndex, true);
         SetModified(true);
         GearRefresh(false);
         GearRestoreTableScroll();
         return;
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_gearMemberTable_cellChanged(int row, int col)
{
   QTableWidgetItem* item = mUI.gearMemberTable->item(row, col);
   if (!item)
   {
      return;
   }

   switch (col)
   {
   case GEAR_COLUMN_NAME:
      {
         mUI.gearMemberTable->verticalHeaderItem(row)->setText(item->text());
      }
      break;
   case GEAR_COLUMN_GUILD:
      {
         QSettings settings(COMPANY_NAME, APPLICATION_NAME);
         settings.beginGroup("gear");
         {
            settings.beginGroup("defaultMemberTableFields");
            settings.setValue("guild", item->text());
            settings.endGroup();
         }
         settings.endGroup();
      }
      break;
   }

   SetModified(true);
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_gearMemberRowColor_cellChanged(int index)
{
   QSettings settings(COMPANY_NAME, APPLICATION_NAME);
   settings.beginGroup("gear");
   {
      settings.beginGroup("defaultMemberTableFields");
      settings.setValue("defaultColor", COLOR_NAME_LIST[index]);
      settings.endGroup();
   }
   settings.endGroup();

   SetModified(true);
   GearRefresh(false);
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_gearMemberPriority_cellChanged(int index)
{
   QSettings settings(COMPANY_NAME, APPLICATION_NAME);
   settings.beginGroup("gear");
   {
      settings.beginGroup("defaultMemberTableFields");
      settings.setValue("priority", PRIORITY_LIST[index]);
      settings.endGroup();
   }
   settings.endGroup();

   SetModified(true);
   GearRefresh(false);
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_gearMemberSchool_cellChanged(int index)
{
   QSettings settings(COMPANY_NAME, APPLICATION_NAME);
   settings.beginGroup("gear");
   {
      settings.beginGroup("defaultMemberTableFields");
      settings.setValue("school", SCHOOL_LIST[index]);
      settings.endGroup();
   }
   settings.endGroup();

   SetModified(true);
   GearRefresh(false);
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_gearMemberAttribute_cellChanged(int index)
{
   QSettings settings(COMPANY_NAME, APPLICATION_NAME);
   settings.beginGroup("gear");
   {
      settings.beginGroup("defaultMemberTableFields");
      settings.setValue("attribute", ATTRIBUTE_LIST[index]);
      settings.endGroup();
   }
   settings.endGroup();

   SetModified(true);
   GearRefresh(false);
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_gearMemberStat_valueChanged()
{
   SetModified(true);
   GearRefresh(false);
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_gearMemberPart_cellChanged(int)
{
   SetModified(true);
   GearRefresh(false);
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_gearMemberComment_cellChanged(int)
{
   SetModified(true);
   GearRefresh(false);
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_gearFilterShowMIACheck_stateChanged(int)
{
   GearRememberTableScroll();
   GearRefresh();
   GearRestoreTableScroll();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_gearFilterShowArmorCheck_stateChanged(int)
{
   GearRememberTableScroll();
   GearRefresh(false);
   GearRestoreTableScroll();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_gearFilterShowJewelryCheck_stateChanged(int)
{
   GearRememberTableScroll();
   GearRefresh(false);
   GearRestoreTableScroll();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_gearFilterShowWeaponsCheck_stateChanged(int)
{
   GearRememberTableScroll();
   GearRefresh(false);
   GearRestoreTableScroll();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_gearFilterSearchEdit_editingFinished()
{
   GearRememberTableScroll();
   GearRefresh();
   GearRestoreTableScroll();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_gearFilterPriorityCombo_currentIndexChanged(int)
{
   GearRememberTableScroll();
   GearRefresh();
   GearRestoreTableScroll();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_gearFilterSchoolCombo_currentIndexChanged(int)
{
   GearRememberTableScroll();
   GearRefresh();
   GearRestoreTableScroll();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_gearFilterAttributeCombo_currentIndexChanged(int)
{
   GearRememberTableScroll();
   GearRefresh();
   GearRestoreTableScroll();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_gearFilterStatCombo_currentIndexChanged(int)
{
   GearRememberTableScroll();
   GearRefresh();
   GearRestoreTableScroll();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_gearFilterClearButton_clicked(bool)
{
   mUI.gearFilterSearchEdit->blockSignals(true);
   mUI.gearFilterPriorityCombo->blockSignals(true);
   mUI.gearFilterSchoolCombo->blockSignals(true);
   mUI.gearFilterAttributeCombo->blockSignals(true);
   mUI.gearFilterStatCombo->blockSignals(true);

   mUI.gearFilterSearchEdit->setText("");
   mUI.gearFilterPriorityCombo->setCurrentIndex(0);
   mUI.gearFilterSchoolCombo->setCurrentIndex(0);
   mUI.gearFilterAttributeCombo->setCurrentIndex(0);
   mUI.gearFilterStatCombo->setCurrentIndex(0);

   mUI.gearFilterStatCombo->blockSignals(false);
   mUI.gearFilterAttributeCombo->blockSignals(false);
   mUI.gearFilterSchoolCombo->blockSignals(false);
   mUI.gearFilterPriorityCombo->blockSignals(false);
   mUI.gearFilterSearchEdit->blockSignals(false);

   GearRememberTableScroll();
   GearRefresh();
   GearRestoreTableScroll();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_gearTableScroll_timeout()
{
   mUI.gearMemberTable->horizontalScrollBar()->setSliderPosition(mGearTableHScroll);
   mUI.gearMemberTable->verticalScrollBar()->setSliderPosition(mGearTableVScroll);
}

////////////////////////////////////////////////////////////////////////////////
int MainWindow::GearGetLoadingCount() const
{
   return mUI.gearMemberTable->rowCount();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::GearClearData()
{
   mUI.gearMemberTable->setRowCount(0);
}

////////////////////////////////////////////////////////////////////////////////
int MainWindow::GearLoadData(pugi::xml_node mainNode, int version, QProgressDialog* progress)
{
   progress->setLabelText("Loading Members...");

   // Clear the table first.
   mUI.gearMemberTable->setRowCount(0);

   // Version 1 is before parts could be configured with
   // a grade value, so by default, make all previously
   // checked grades = silver.
   int defaultGrade = -1;
   if (version == 1)
   {
      defaultGrade = DEFAULT_GRADE;
   }

   pugi::xml_node membersNode = mainNode.child(XML_MEMBERS_ELEMENT);
   if (!membersNode)
   {
      return LOAD_ERROR_FAILED;
   }

   // Restore filter settings.
   pugi::xml_node filtersNode = membersNode.child(XML_FILTERS_ELEMENT);
   if (filtersNode)
   {
      mUI.gearFilterShowMIACheck->setChecked(!strcmp(filtersNode.child_value(XML_MIA_ELEMENT), "True"));
      mUI.gearFilterShowArmorCheck->setChecked(!strcmp(filtersNode.child_value(XML_ARMOR_ELEMENT), "True"));
      mUI.gearFilterShowJewelryCheck->setChecked(!strcmp(filtersNode.child_value(XML_JEWELRY_ELEMENT), "True"));
      mUI.gearFilterShowWeaponsCheck->setChecked(!strcmp(filtersNode.child_value(XML_WEAPONS_ELEMENT), "True"));
      mUI.gearFilterSearchEdit->setText(filtersNode.child_value(XML_TEXT_ELEMENT));
      mUI.gearFilterPriorityCombo->setCurrentIndex(mUI.gearFilterPriorityCombo->findText(filtersNode.child_value(XML_PRIORITY_ELEMENT)));
      mUI.gearFilterSchoolCombo->setCurrentIndex(mUI.gearFilterSchoolCombo->findText(filtersNode.child_value(XML_SCHOOL_ELEMENT)));
      mUI.gearFilterAttributeCombo->setCurrentIndex(mUI.gearFilterAttributeCombo->findText(filtersNode.child_value(XML_ATTRIBUTE_ELEMENT)));
      mUI.gearFilterStatCombo->setCurrentIndex(mUI.gearFilterStatCombo->findText(filtersNode.child_value(XML_STAT_ELEMENT)));
   }

   QList<int> hiddenRows;

   pugi::xml_node memberNode = membersNode.child(XML_MEMBER_ELEMENT);
   while (memberNode)
   {
      QString color = memberNode.child_value(XML_COLOR_ELEMENT);
      QString name = memberNode.child_value(XML_NAME_ELEMENT);
      QString guild = memberNode.child_value(XML_GUILD_ELEMENT);
      QString priority = memberNode.child_value(XML_PRIORITY_ELEMENT);
      QString school = memberNode.child_value(XML_SCHOOL_ELEMENT);
      QString attribute = memberNode.child_value(XML_ATTRIBUTE_ELEMENT);
      QStringList stats;
      pugi::xml_node statNode = memberNode.child(XML_STATS_ELEMENT).child(XML_STAT_ELEMENT);
      while (statNode)
      {
         stats.push_back(statNode.child_value());
         statNode = statNode.next_sibling(XML_STAT_ELEMENT);
      }
      QStringList parts;
      pugi::xml_node partNode = memberNode.child(XML_PARTS_ELEMENT).child(XML_PART_ELEMENT);
      while (partNode)
      {
         parts.push_back(partNode.child_value());
         partNode = partNode.next_sibling(XML_PART_ELEMENT);
      }

      bool rowHidden = !strcmp(memberNode.child_value(XML_HIDDEN_ELEMENT), "true");

      QString comment;
      QString detail;
      pugi::xml_node commentNode = memberNode.child(XML_COMMENT_ELEMENT);
      if (commentNode)
      {
         comment = commentNode.child_value(XML_COMMENT_SHORT_ELEMENT);
         detail = commentNode.child_value(XML_COMMENT_DETAIL_ELEMENT);
      }

      int row = GearAddMemberRow(color, name, guild, priority, school, attribute, stats, MakePartList(parts, defaultGrade), comment, detail);
      if (rowHidden)
      {
         hiddenRows.push_back(row);
      }

      progress->setValue(progress->value() + 1);
      QApplication::processEvents();
      if (progress->wasCanceled())
      {
         return LOAD_ERROR_CANCELLED;
      }

      memberNode = memberNode.next_sibling(XML_MEMBER_ELEMENT);
   }

   // Now hide all previously hidden rows.
   for (int rowIndex = 0; rowIndex < hiddenRows.count(); ++rowIndex)
   {
      mUI.gearMemberTable->setRowHidden(hiddenRows[rowIndex], true);
   }

   GearRefresh(false);
   GearResetTableScroll(false);
   return LOAD_ERROR_NONE;
}

////////////////////////////////////////////////////////////////////////////////
bool MainWindow::GearSaveData(pugi::xml_node mainNode) const
{
   pugi::xml_node membersNode = mainNode.append_child(XML_MEMBERS_ELEMENT);

   // Store filter settings.
   pugi::xml_node filtersNode = membersNode.append_child(XML_FILTERS_ELEMENT);
   bool filterMIA          = mUI.gearFilterShowMIACheck->isChecked();
   bool filterArmor        = mUI.gearFilterShowArmorCheck->isChecked();
   bool filterJewelry      = mUI.gearFilterShowJewelryCheck->isChecked();
   bool filterWeapons      = mUI.gearFilterShowWeaponsCheck->isChecked();
   QString filterText      = mUI.gearFilterSearchEdit->text();
   QString filterPriority  = mUI.gearFilterPriorityCombo->currentText();
   QString filterSchool    = mUI.gearFilterSchoolCombo->currentText();
   QString filterAttribute = mUI.gearFilterAttributeCombo->currentText();
   QString filterStat      = mUI.gearFilterStatCombo->currentText();
   filtersNode.append_child(XML_MIA_ELEMENT).append_child(pugi::node_pcdata).set_value(filterMIA? "True": "False");
   filtersNode.append_child(XML_ARMOR_ELEMENT).append_child(pugi::node_pcdata).set_value(filterArmor? "True": "False");
   filtersNode.append_child(XML_JEWELRY_ELEMENT).append_child(pugi::node_pcdata).set_value(filterJewelry? "True": "False");
   filtersNode.append_child(XML_WEAPONS_ELEMENT).append_child(pugi::node_pcdata).set_value(filterWeapons? "True": "False");
   filtersNode.append_child(XML_TEXT_ELEMENT).append_child(pugi::node_pcdata).set_value(filterText.toStdString().c_str());
   filtersNode.append_child(XML_PRIORITY_ELEMENT).append_child(pugi::node_pcdata).set_value(filterPriority.toStdString().c_str());
   filtersNode.append_child(XML_SCHOOL_ELEMENT).append_child(pugi::node_pcdata).set_value(filterSchool.toStdString().c_str());
   filtersNode.append_child(XML_ATTRIBUTE_ELEMENT).append_child(pugi::node_pcdata).set_value(filterAttribute.toStdString().c_str());
   filtersNode.append_child(XML_STAT_ELEMENT).append_child(pugi::node_pcdata).set_value(filterStat.toStdString().c_str());

   int rowCount = mUI.gearMemberTable->rowCount();
   for (int rowIndex = 0; rowIndex < rowCount; ++rowIndex)
   {
      bool rowHidden = mUI.gearMemberTable->isRowHidden(rowIndex);

      // Color
      int colorIndex = 0;
      QComboBox* colorCombo = dynamic_cast<QComboBox*>(mUI.gearMemberTable->cellWidget(rowIndex, GEAR_COLUMN_COLOR));
      if (colorCombo)
      {
         colorIndex = colorCombo->currentIndex();
      }

      QString color = COLOR_NAME_LIST[0];
      if (colorIndex > -1 && colorIndex < COLOR_COUNT)
      {
         color = COLOR_NAME_LIST[colorIndex];
      }

      // Name
      QString name = mUI.gearMemberTable->item(rowIndex, GEAR_COLUMN_NAME)->text();

      // Guild
      QString guild = mUI.gearMemberTable->item(rowIndex, GEAR_COLUMN_GUILD)->text();

      // Priority
      int priorityIndex = 0;
      QComboBox* priorityCombo = dynamic_cast<QComboBox*>(mUI.gearMemberTable->cellWidget(rowIndex, GEAR_COLUMN_PRIORITY));
      if (priorityCombo)
      {
         priorityIndex = priorityCombo->currentIndex();
      }

      QString priority = PRIORITY_LIST[0];
      if (priorityIndex > -1 && priorityIndex < PRIORITY_COUNT)
      {
         priority = PRIORITY_LIST[priorityIndex];
      }

      // School
      int schoolIndex = 0;
      QComboBox* schoolCombo = dynamic_cast<QComboBox*>(mUI.gearMemberTable->cellWidget(rowIndex, GEAR_COLUMN_SCHOOL));
      if (schoolCombo)
      {
         schoolIndex = schoolCombo->currentIndex();
      }

      QString school = SCHOOL_LIST[0];
      if (schoolIndex > -1 && schoolIndex < SCHOOL_COUNT)
      {
         school = SCHOOL_LIST[schoolIndex];
      }

      // Attribute
      int attributeIndex = 0;
      QComboBox* attributeCombo = dynamic_cast<QComboBox*>(mUI.gearMemberTable->cellWidget(rowIndex, GEAR_COLUMN_ATTRIBUTE));
      if (attributeCombo)
      {
         attributeIndex = attributeCombo->currentIndex();
      }

      QString attribute = ATTRIBUTE_LIST[0];
      if (attributeIndex > -1 && attributeIndex < ATTRIBUTE_COUNT)
      {
         attribute = ATTRIBUTE_LIST[attributeIndex];
      }

      // Stats
      QStringList stats;
      MultiComboBox* statsCombo = dynamic_cast<MultiComboBox*>(mUI.gearMemberTable->cellWidget(rowIndex, GEAR_COLUMN_STATS));
      if (statsCombo)
      {
         stats = statsCombo->GetSelection();
      }

      // Parts
      QStringList parts;
      for (int partIndex = 0; partIndex < PART_COUNT; ++partIndex)
      {
         PartWidgetItem* partWidget = dynamic_cast<PartWidgetItem*>(mUI.gearMemberTable->cellWidget(rowIndex, GEAR_COLUMN_PART_START_INDEX + partIndex));
         if (partWidget)
         {
            parts.push_back(PART_LIST[partIndex] + ":" + partWidget->GetValue());
         }
      }

      // Comment
      QString commentString;
      QString detailString;
      CommentWidgetItem* commentWidget = dynamic_cast<CommentWidgetItem*>(mUI.gearMemberTable->cellWidget(rowIndex, GEAR_COLUMN_COMMENT));
      if (commentWidget)
      {
         commentString = commentWidget->GetComment();
         detailString = commentWidget->GetDetail();
      }

      pugi::xml_node memberNode = membersNode.append_child(XML_MEMBER_ELEMENT);
      memberNode.append_child(XML_COLOR_ELEMENT).append_child(pugi::node_pcdata).set_value(color.toStdString().c_str());
      memberNode.append_child(XML_NAME_ELEMENT).append_child(pugi::node_pcdata).set_value(name.toStdString().c_str());
      memberNode.append_child(XML_GUILD_ELEMENT).append_child(pugi::node_pcdata).set_value(guild.toStdString().c_str());
      memberNode.append_child(XML_PRIORITY_ELEMENT).append_child(pugi::node_pcdata).set_value(priority.toStdString().c_str());
      memberNode.append_child(XML_SCHOOL_ELEMENT).append_child(pugi::node_pcdata).set_value(school.toStdString().c_str());
      memberNode.append_child(XML_ATTRIBUTE_ELEMENT).append_child(pugi::node_pcdata).set_value(attribute.toStdString().c_str());
      pugi::xml_node statsNode = memberNode.append_child(XML_STATS_ELEMENT);
      for (int statIndex = 0; statIndex < stats.count(); ++statIndex)
      {
         statsNode.append_child(XML_STAT_ELEMENT).append_child(pugi::node_pcdata).set_value(stats[statIndex].toStdString().c_str());
      }
      pugi::xml_node partsNode = memberNode.append_child(XML_PARTS_ELEMENT);
      for (int partIndex = 0; partIndex < parts.count(); ++partIndex)
      {
         partsNode.append_child(XML_PART_ELEMENT).append_child(pugi::node_pcdata).set_value(parts[partIndex].toStdString().c_str());
      }
      memberNode.append_child(XML_HIDDEN_ELEMENT).append_child(pugi::node_pcdata).set_value(rowHidden? "true": "false");
      pugi::xml_node commentNode = memberNode.append_child(XML_COMMENT_ELEMENT);
      commentNode.append_child(XML_COMMENT_SHORT_ELEMENT).append_child(pugi::node_pcdata).set_value(commentString.toStdString().c_str());
      commentNode.append_child(XML_COMMENT_DETAIL_ELEMENT).append_child(pugi::node_pcdata).set_value(detailString.toStdString().c_str());
   }
   return true;
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::SetupUIGear()
{
   // Member table
   QStringList labels;
   labels.push_back("Color");
   labels.push_back("Name");
   labels.push_back("Guild");
   labels.push_back("Priority");
   labels.push_back("School");
   labels.push_back("Attribute");
   labels.push_back("Stats Desired");
   labels += PART_LIST;
   labels.push_back("Delete");
   labels.push_back("Hide");
   labels.push_back("Comment");
   mUI.gearMemberTable->setColumnCount(labels.count());
   mUI.gearMemberTable->setHorizontalHeaderLabels(labels);
   mUI.gearMemberTable->verticalHeader()->hide();
   mUI.gearMemberTable->setSortingEnabled(true);
   mUI.gearMemberTable->resizeColumnsToContents();
   mUI.gearMemberTable->setSelectionMode(QAbstractItemView::NoSelection);

   mUI.gearMemberTable->setColumnWidth(GEAR_COLUMN_COLOR, 60);
   mUI.gearMemberTable->setColumnWidth(GEAR_COLUMN_NAME, 100);
   mUI.gearMemberTable->setColumnWidth(GEAR_COLUMN_GUILD, 100);
   mUI.gearMemberTable->setColumnWidth(GEAR_COLUMN_PRIORITY, 50);
   mUI.gearMemberTable->setColumnWidth(GEAR_COLUMN_SCHOOL, 110);
   mUI.gearMemberTable->setColumnWidth(GEAR_COLUMN_ATTRIBUTE, 60);
   mUI.gearMemberTable->setColumnWidth(GEAR_COLUMN_STATS, 130);
   for (int columnIndex = 0; columnIndex < PART_COUNT; ++columnIndex)
   {
      if (columnIndex >= WEAPON_START_INDEX)
      {
         mUI.gearMemberTable->setColumnWidth(GEAR_COLUMN_PART_START_INDEX + columnIndex, 130);
      }
      else
      {
         mUI.gearMemberTable->setColumnWidth(GEAR_COLUMN_PART_START_INDEX + columnIndex, 40);
      }
   }
   mUI.gearMemberTable->setColumnWidth(GEAR_COLUMN_DELETE, 50);
   mUI.gearMemberTable->setColumnWidth(GEAR_COLUMN_HIDE, 50);
   mUI.gearMemberTable->setColumnWidth(GEAR_COLUMN_COMMENT, 150);

   // Summary
   labels.clear();
   labels += GRADE_LIST;
   mUI.gearSummaryTable->setColumnCount(labels.count());
   mUI.gearSummaryTable->setHorizontalHeaderLabels(labels);
   mUI.gearSummaryTable->resizeColumnsToContents();

   labels.clear();
   labels += PART_LIST;
   mUI.gearSummaryTable->setRowCount(labels.count());
   mUI.gearSummaryTable->setVerticalHeaderLabels(labels);

   // Filters
   mUI.gearFilterShowMIACheck->setChecked(true);
   mUI.gearFilterShowArmorCheck->setChecked(true);
   mUI.gearFilterShowJewelryCheck->setChecked(true);
   mUI.gearFilterShowWeaponsCheck->setChecked(true);

   mUI.gearFilterPriorityCombo->addItem("None");
   mUI.gearFilterPriorityCombo->addItems(PRIORITY_LIST);

   mUI.gearFilterSchoolCombo->addItem("None");
   mUI.gearFilterSchoolCombo->addItems(SCHOOL_LIST);

   mUI.gearFilterAttributeCombo->addItem("None");
   mUI.gearFilterAttributeCombo->addItems(ATTRIBUTE_LIST);

   mUI.gearFilterStatCombo->addItem("None");
   mUI.gearFilterStatCombo->addItems(STAT_LIST);

   // Default splitter sizes.
   QList<int> sizes;
   sizes.push_back((width() / 4) * 3);
   sizes.push_back((width() / 4) * 1);
   mUI.gearMainSplitter->setSizes(sizes);
}

////////////////////////////////////////////////////////////////////////////////
bool MainWindow::GearExport(const QString& filename)
{
   pugi::xml_document document;

   QFileInfo titleInfo = filename;
   QString title = titleInfo.fileName();

   bool isJS = false;
   if (title.contains(".js", Qt::CaseInsensitive))
   {
      isJS = true;
      title = title.remove(".js", Qt::CaseInsensitive);
   }
   else
   {
      title = title.remove(".txt", Qt::CaseInsensitive);
   }

   int lowestGrade = GRADE_COUNT;
   int highestGrade = 0;

   // Header table.
   pugi::xml_node tableNode = document.append_child(EXPORT_TABLE_ELEMENT);
   tableNode.append_attribute(EXPORT_STYLE_ELEMENT).set_value("font-size:11px; text-align:center;");
   tableNode.append_attribute(EXPORT_WIDTH_ELEMENT).set_value("100%");
   tableNode.append_attribute(EXPORT_CELLSPACING_ELEMENT).set_value("0");
   tableNode.append_attribute(EXPORT_CELLPADDING_ELEMENT).set_value("0");
   pugi::xml_node headerNode = tableNode.append_child(EXPORT_TABLE_ROW_ELEMENT).append_child(EXPORT_TABLE_COL_ELEMENT);
   headerNode.append_attribute(EXPORT_BACKGROUND_COLOR_ELEMENT).set_value("#555");
   pugi::xml_node fontNode = headerNode.append_child(EXPORT_FONT_ELEMENT);
   fontNode.append_attribute(EXPORT_COLOR_ELEMENT).set_value("FFFFFF");
   fontNode.append_child(pugi::node_pcdata).set_value(title.toStdString().c_str());

   // Table
   tableNode = document.append_child(EXPORT_TABLE_ELEMENT);
   tableNode.append_attribute(EXPORT_STYLE_ELEMENT).set_value("font-size:11px; color:#aaa; text-align:center;");
   tableNode.append_attribute(EXPORT_WIDTH_ELEMENT).set_value("100%");
   tableNode.append_attribute(EXPORT_CELLSPACING_ELEMENT).set_value("0");
   tableNode.append_attribute(EXPORT_CELLPADDING_ELEMENT).set_value("0");

   // Header data.
   headerNode = tableNode.append_child(EXPORT_TABLE_ROW_ELEMENT);
   headerNode.append_attribute(EXPORT_BACKGROUND_COLOR_ELEMENT).set_value("#444");
   headerNode.append_child(EXPORT_TABLE_COL_ELEMENT).append_child(pugi::node_pcdata).set_value("Member");
   headerNode.append_child(EXPORT_TABLE_COL_ELEMENT).append_child(pugi::node_pcdata).set_value("Internal");
   for (int partIndex = 0; partIndex < PART_COUNT; ++partIndex)
   {
      if (!mUI.gearMemberTable->isColumnHidden(GEAR_COLUMN_PART_START_INDEX + partIndex))
      {
         headerNode.append_child(EXPORT_TABLE_COL_ELEMENT).append_child(pugi::node_pcdata).set_value(PART_LIST[partIndex].toStdString().c_str());
      }
   }

   // Keep track of a counter for each item type.
   QList< QList<int> > needCount;
   QList<int> subCount;
   for (int priorityIndex = 0; priorityIndex < GRADE_COUNT; ++priorityIndex)
   {
      subCount.push_back(0);
   }
   for (int partIndex = 0; partIndex < PART_COUNT; ++partIndex)
   {
      needCount.push_back(subCount);
   }

   // Table data.
   pugi::xml_node rowNode;
   int memberCount = 0;
   int rowCount = mUI.gearMemberTable->rowCount();
   for (int rowIndex = 0; rowIndex < rowCount; ++rowIndex)
   {
      // Ignore hidden rows.
      if (mUI.gearMemberTable->isRowHidden(rowIndex))
      {
         continue;
      }

      rowNode = tableNode.append_child(EXPORT_TABLE_ROW_ELEMENT);

      if (memberCount % 2)
      {
         rowNode.append_attribute(EXPORT_BACKGROUND_COLOR_ELEMENT).set_value("#222");
      }

      memberCount++;

      // Name
      QString name = mUI.gearMemberTable->item(rowIndex, GEAR_COLUMN_NAME)->text();
      rowNode.append_child(EXPORT_TABLE_COL_ELEMENT).append_child(pugi::node_pcdata).set_value(name.toStdString().c_str());

      // Attribute
      QString attribute = ATTRIBUTE_LIST[0];
      QComboBox* attributeCombo = dynamic_cast<QComboBox*>(mUI.gearMemberTable->cellWidget(rowIndex, GEAR_COLUMN_ATTRIBUTE));
      if (attributeCombo)
      {
         attribute = ATTRIBUTE_LIST[attributeCombo->currentIndex()];
      }
      rowNode.append_child(EXPORT_TABLE_COL_ELEMENT).append_child(pugi::node_pcdata).set_value(attribute.toStdString().c_str());

      // Parts
      QStringList parts;
      for (int partIndex = 0; partIndex < PART_COUNT; ++partIndex)
      {
         // Skip hidden columns.
         if (mUI.gearMemberTable->isColumnHidden(GEAR_COLUMN_PART_START_INDEX + partIndex))
         {
            continue;
         }

         int grade = -1;
         int weapon = -1;
         PartWidgetItem* partWidget = dynamic_cast<PartWidgetItem*>(mUI.gearMemberTable->cellWidget(rowIndex, GEAR_COLUMN_PART_START_INDEX + partIndex));
         if (partWidget)
         {
            grade = partWidget->GetGrade() - 1;
            weapon = partWidget->GetWeaponType() - 1;
         }

         QString val;
         if (grade > -1 && grade < GRADE_COUNT)
         {
            val += GRADE_LIST[grade];

            if (lowestGrade > grade)
            {
               lowestGrade = grade;
            }
            if (highestGrade < grade)
            {
               highestGrade = grade;
            }
         }
         else
         {
            val += "-";
         }

         if (weapon > -1 && weapon < WEAPON_TYPE_COUNT)
         {
            val += " (";
            val += WEAPON_TYPE_LIST[weapon];
            val += ")";
         }

         QColor color = GetColorForGrade(grade);
         fontNode = rowNode.append_child(EXPORT_TABLE_COL_ELEMENT).append_child(EXPORT_FONT_ELEMENT);
         fontNode.append_attribute(EXPORT_COLOR_ELEMENT).set_value(color.name().toStdString().c_str());
         fontNode.append_child(pugi::node_pcdata).set_value(val.toStdString().c_str());

         // Increment the need count of this part for all grades above the currently
         // selected grade.  If this part is a weapon, only increment if the
         // weapon type is defined.
         if (!partWidget->HasWeapon() || partWidget->GetWeaponType() > 0)
         {
            for (grade = grade + 1; grade < GRADE_COUNT; ++grade)
            {
               needCount[partIndex][grade]++;
            }
         }
      }
   }

   // Add a spacer row.
   rowNode = tableNode.append_child(EXPORT_TABLE_ROW_ELEMENT);
   rowNode.append_attribute(EXPORT_BACKGROUND_COLOR_ELEMENT).set_value("#444");
   rowNode.append_child(EXPORT_TABLE_COL_ELEMENT).append_child(pugi::node_pcdata).set_value("---");
   rowNode.append_child(EXPORT_TABLE_COL_ELEMENT).append_child(pugi::node_pcdata).set_value("---");
   for (int partIndex = 0; partIndex < PART_COUNT; ++partIndex)
   {
      if (!mUI.gearMemberTable->isColumnHidden(GEAR_COLUMN_PART_START_INDEX + partIndex))
      {
         rowNode.append_child(EXPORT_TABLE_COL_ELEMENT).append_child(EXPORT_BOLD_ELEMENT).append_child(pugi::node_pcdata).set_value("---");
      }
   }

   // Always show one higher grade than what is available, if able.
   highestGrade++;
   if (highestGrade >= GRADE_COUNT)
   {
      highestGrade = GRADE_COUNT - 1;
   }

   // Output a final set of table rows that lists out the total counts.
   int gradeCount = 0;
   for (int grade = lowestGrade; grade <= highestGrade; ++grade)
   {
      QColor color = GetColorForGrade(grade);
      rowNode = tableNode.append_child(EXPORT_TABLE_ROW_ELEMENT);
      if (gradeCount % 2)
      {
         rowNode.append_attribute(EXPORT_BACKGROUND_COLOR_ELEMENT).set_value("#444");
      }
      else
      {
         rowNode.append_attribute(EXPORT_BACKGROUND_COLOR_ELEMENT).set_value("#555");
      }
      gradeCount++;
      fontNode = rowNode.append_child(EXPORT_TABLE_COL_ELEMENT).append_child(EXPORT_FONT_ELEMENT);
      fontNode.append_attribute(EXPORT_COLOR_ELEMENT).set_value(color.name().toStdString().c_str());
      fontNode.append_child(pugi::node_pcdata).set_value(QString(GRADE_LIST[grade] + " needed:").toStdString().c_str());
      rowNode.append_child(EXPORT_TABLE_COL_ELEMENT).append_child(pugi::node_pcdata).set_value("---");
      for (int partIndex = 0; partIndex < PART_COUNT; ++partIndex)
      {
         if (!mUI.gearMemberTable->isColumnHidden(GEAR_COLUMN_PART_START_INDEX + partIndex))
         {
            fontNode = rowNode.append_child(EXPORT_TABLE_COL_ELEMENT).append_child(EXPORT_FONT_ELEMENT);
            fontNode.append_attribute(EXPORT_COLOR_ELEMENT).set_value(color.name().toStdString().c_str());
            fontNode.append_child(EXPORT_BOLD_ELEMENT).append_child(pugi::node_pcdata).set_value(QString::number(needCount[partIndex][grade]).toStdString().c_str());
         }
      }
   }

   document.save_file(filename.toStdString().c_str(), "    ", pugi::format_no_declaration | pugi::format_indent);

   // Now we have to re-open the script we just saved and format it into JavaScript format instead of plain XML.
   if (isJS)
   {
      QFile file(filename);
      if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
      {
         return false;
      }

      QTextStream stream(&file);

      QStringList lines;
      while (!stream.atEnd())
      {
         lines.push_back(stream.readLine());
      }

      file.close();

      if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
      {
         return false;
      }

      file.write("document.write(\'\\\n");

      for (int index = 0; index < lines.count(); ++index)
      {
         file.write(QString(lines[index] + "\\\n").toStdString().c_str());
      }
      file.write("\');");

      file.close();
   }

   return true;
}

////////////////////////////////////////////////////////////////////////////////
int MainWindow::GearAddMemberRow(const QString& color, const QString& name, const QString& guild, const QString& priority, const QString& school, const QString& attribute, const QStringList& stats, const QList<PartData>& parts, const QString& comment, const QString& detail)
{
   mUI.gearMemberTable->blockSignals(true);

   mUI.gearMemberTable->sortItems(GEAR_COLUMN_DELETE, Qt::AscendingOrder);
   mUI.gearMemberTable->setSortingEnabled(false);
   int row = mUI.gearMemberTable->rowCount();
   mUI.gearMemberTable->insertRow(row);

   ColoredComboBox* colorCombo = new ColoredComboBox();
   ComboSortItem* sortItem = new ComboSortItem(colorCombo);
   colorCombo->AddItems(COLOR_NAME_LIST, COLOR_LIST);
   colorCombo->setCurrentIndex(colorCombo->findText(color));
   mUI.gearMemberTable->setItem(row, GEAR_COLUMN_COLOR, sortItem);
   mUI.gearMemberTable->setCellWidget(row, GEAR_COLUMN_COLOR, colorCombo);
   connect(colorCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(on_gearMemberRowColor_cellChanged(int)));

   QTableWidgetItem* nameWidget = new QTableWidgetItem(name);
   QTableWidgetItem* guildWidget = new QTableWidgetItem(guild);
   mUI.gearMemberTable->setItem(row, GEAR_COLUMN_NAME, nameWidget);
   mUI.gearMemberTable->setItem(row, GEAR_COLUMN_GUILD, guildWidget);

   QTableWidgetItem* headerNameWidget = new QTableWidgetItem(name);
   mUI.gearMemberTable->setVerticalHeaderItem(row, headerNameWidget);

   NoScrollComboBox* priorityCombo = new NoScrollComboBox();
   sortItem = new ComboSortItem(priorityCombo);
   priorityCombo->addItems(PRIORITY_LIST);
   priorityCombo->setCurrentIndex(priorityCombo->findText(priority));
   mUI.gearMemberTable->setItem(row, GEAR_COLUMN_PRIORITY, sortItem);
   mUI.gearMemberTable->setCellWidget(row, GEAR_COLUMN_PRIORITY, priorityCombo);
   connect(priorityCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(on_gearMemberPriority_cellChanged(int)));

   NoScrollComboBox* schoolCombo = new NoScrollComboBox();
   sortItem = new ComboSortItem(schoolCombo);
   schoolCombo->addItems(SCHOOL_LIST);
   schoolCombo->setCurrentIndex(schoolCombo->findText(school));
   mUI.gearMemberTable->setItem(row, GEAR_COLUMN_SCHOOL, sortItem);
   mUI.gearMemberTable->setCellWidget(row, GEAR_COLUMN_SCHOOL, schoolCombo);
   connect(schoolCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(on_gearMemberSchool_cellChanged(int)));

   NoScrollComboBox* attributeCombo = new NoScrollComboBox();
   sortItem = new ComboSortItem(attributeCombo);
   attributeCombo->addItems(ATTRIBUTE_LIST);
   attributeCombo->setCurrentIndex(attributeCombo->findText(attribute));
   mUI.gearMemberTable->setItem(row, GEAR_COLUMN_ATTRIBUTE, sortItem);
   mUI.gearMemberTable->setCellWidget(row, GEAR_COLUMN_ATTRIBUTE, attributeCombo);
   connect(attributeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(on_gearMemberAttribute_cellChanged(int)));

   MultiComboBox* statsCombo = new MultiComboBox();
   MultiComboSortItem* multiSortItem = new MultiComboSortItem(statsCombo);
   statsCombo->addItems(STAT_LIST);
   statsCombo->SetSelection(stats);
   mUI.gearMemberTable->setItem(row, GEAR_COLUMN_STATS, multiSortItem);
   mUI.gearMemberTable->setCellWidget(row, GEAR_COLUMN_STATS, statsCombo);
   connect(statsCombo, SIGNAL(valueChanged()), this, SLOT(on_gearMemberStat_valueChanged()));

   for (int partIndex = 0; partIndex < PART_COUNT; ++partIndex)
   {
      bool isWeapon = (partIndex >= WEAPON_START_INDEX);
      PartWidgetItem* partWidget = new PartWidgetItem(isWeapon);
      int index = parts.indexOf(PartData(PART_LIST[partIndex]));
      if (index > -1 && index < parts.count())
      {
         const PartData& data = parts[index];
         partWidget->SetGrade(data.grade);
         partWidget->SetWeaponType(data.weaponType);
      }

      mUI.gearMemberTable->setItem(row, GEAR_COLUMN_PART_START_INDEX + partIndex, partWidget);
      mUI.gearMemberTable->setCellWidget(row, GEAR_COLUMN_PART_START_INDEX + partIndex, partWidget);
      connect(partWidget, SIGNAL(stateChanged(int)), this, SLOT(on_gearMemberPart_cellChanged(int)));
   }

   QPushButton* deleteButton = new QPushButton("Delete");
   QTableWidgetItem* indexSortItem = new QTableWidgetItem();
   mUI.gearMemberTable->setItem(row, GEAR_COLUMN_DELETE, indexSortItem); 
   mUI.gearMemberTable->setCellWidget(row, GEAR_COLUMN_DELETE, deleteButton);
   connect(deleteButton, SIGNAL(clicked(bool)), this, SLOT(on_gearRemoveMemberButton_clicked(bool)));

   QPushButton* hideButton = new QPushButton("Hide");
   mUI.gearMemberTable->setCellWidget(row, GEAR_COLUMN_HIDE, hideButton);
   connect(hideButton, SIGNAL(clicked(bool)), this, SLOT(on_gearHideMemberButton_clicked(bool)));

   CommentWidgetItem* commentWidget = new CommentWidgetItem();
   commentWidget->SetNameWidget(nameWidget);
   commentWidget->SetGuildWidget(guildWidget);
   commentWidget->SetComment(comment);
   commentWidget->SetDetail(detail);
   mUI.gearMemberTable->setItem(row, GEAR_COLUMN_COMMENT, commentWidget);
   mUI.gearMemberTable->setCellWidget(row, GEAR_COLUMN_COMMENT, commentWidget);
   connect(commentWidget, SIGNAL(edited(int)), this, SLOT(on_gearMemberComment_cellChanged(int)));

   mUI.gearMemberTable->selectRow(row);
   mUI.gearMemberTable->setSortingEnabled(true);

   SetModified(true);

   mUI.gearMemberTable->blockSignals(false);
   return row;
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::GearRefresh(bool applyFilters)
{
   mUI.gearMemberTable->blockSignals(true);

   mUI.gearMemberTable->sortItems(GEAR_COLUMN_DELETE, Qt::AscendingOrder);
   mUI.gearMemberTable->setSortingEnabled(false);

   bool filterMIA      = !mUI.gearFilterShowMIACheck->isChecked();
   bool filterArmor    = !mUI.gearFilterShowArmorCheck->isChecked();
   bool filterJewelry  = !mUI.gearFilterShowJewelryCheck->isChecked();
   bool filterWeapons  = !mUI.gearFilterShowWeaponsCheck->isChecked();

   QString filterText  = mUI.gearFilterSearchEdit->text();
   int filterPriority  = mUI.gearFilterPriorityCombo->currentIndex() - 1;
   int filterSchool    = mUI.gearFilterSchoolCombo->currentIndex() - 1;
   int filterAttribute = mUI.gearFilterAttributeCombo->currentIndex() - 1;
   int filterStat      = mUI.gearFilterStatCombo->currentIndex() - 1;

   QList< QList<int> > needCount;
   QList<int> subCount;
   for (int priorityIndex = 0; priorityIndex < GRADE_COUNT; ++priorityIndex)
   {
      subCount.push_back(0);
   }
   for (int partIndex = 0; partIndex < PART_COUNT; ++partIndex)
   {
      needCount.push_back(subCount);

      // Set part column visibility.
      bool hideColumn = true;
      if (partIndex < TAILOR_LIST.count())
      {
         hideColumn = filterArmor;
      }
      else if (partIndex - TAILOR_LIST.count() < JEWELRY_LIST.count())
      {
         hideColumn = filterJewelry;
      }
      else if (partIndex - TAILOR_LIST.count() - JEWELRY_LIST.count() < WEAPON_LIST.count())
      {
         hideColumn = filterWeapons;
      }

      mUI.gearMemberTable->setColumnHidden(GEAR_COLUMN_PART_START_INDEX + partIndex, hideColumn);

      mUI.gearSummaryTable->setRowHidden(partIndex, hideColumn);
   }
   

   int memberCount = 0;
   int rowCount = mUI.gearMemberTable->rowCount();
   for (int rowIndex = 0; rowIndex < rowCount; ++rowIndex)
   {
      // If we are doing any filters, check them.
      bool showRow = true;
      if (applyFilters)
      {
         if (!filterText.isEmpty())
         {
            bool isTextFound = false;
            QTableWidgetItem* nameItem = dynamic_cast<QTableWidgetItem*>(mUI.gearMemberTable->item(rowIndex, GEAR_COLUMN_NAME));
            if (nameItem)
            {
               if (nameItem->text().contains(filterText, Qt::CaseInsensitive))
               {
                  isTextFound = true;
               }
            }
            QTableWidgetItem* guildItem = dynamic_cast<QTableWidgetItem*>(mUI.gearMemberTable->item(rowIndex, GEAR_COLUMN_GUILD));
            if (guildItem)
            {
               if (guildItem->text().contains(filterText, Qt::CaseInsensitive))
               {
                  isTextFound = true;
               }
            }

            if (!isTextFound)
            {
               showRow = false;
            }
         }

         if (filterPriority > -1 || filterMIA)
         {
            QComboBox* priorityCombo = dynamic_cast<QComboBox*>(mUI.gearMemberTable->cellWidget(rowIndex, GEAR_COLUMN_PRIORITY));
            if (priorityCombo)
            {
               if (filterPriority != priorityCombo->currentIndex() &&
                  (filterPriority > -1 || (filterMIA && priorityCombo->currentIndex() == 0)))
               {
                  showRow = false;
               }
            }
         }

         if (filterSchool > -1)
         {
            QComboBox* schoolCombo = dynamic_cast<QComboBox*>(mUI.gearMemberTable->cellWidget(rowIndex, GEAR_COLUMN_SCHOOL));
            if (schoolCombo)
            {
               if (filterSchool != schoolCombo->currentIndex())
               {
                  showRow = false;
               }
            }
         }

         if (filterAttribute > -1)
         {
            QComboBox* attributeCombo = dynamic_cast<QComboBox*>(mUI.gearMemberTable->cellWidget(rowIndex, GEAR_COLUMN_ATTRIBUTE));
            if (attributeCombo)
            {
               if (filterAttribute != attributeCombo->currentIndex())
               {
                  showRow = false;
               }
            }
         }

         if (filterStat > -1)
         {
            MultiComboBox* statCombo = dynamic_cast<MultiComboBox*>(mUI.gearMemberTable->cellWidget(rowIndex, GEAR_COLUMN_STATS));
            if (statCombo)
            {
               if (!statCombo->IsIndexSelected(filterStat))
               {
                  showRow = false;
               }
            }
         }

         mUI.gearMemberTable->setRowHidden(rowIndex, !showRow);
      }
      else
      {
         showRow = !mUI.gearMemberTable->isRowHidden(rowIndex);
      }

      if (showRow)
      {
         memberCount++;
         // Set the row color.
         int color = -1;
         QComboBox* colorCombo = dynamic_cast<QComboBox*>(mUI.gearMemberTable->cellWidget(rowIndex, GEAR_COLUMN_COLOR));
         if (colorCombo)
         {
            color = colorCombo->currentIndex();
         }

         if (color > -1 && color < COLOR_COUNT)
         {
            for (int colIndex = 0; colIndex < mUI.gearMemberTable->columnCount(); ++colIndex)
            {
               mUI.gearMemberTable->model()->setData(
                  mUI.gearMemberTable->model()->index(
                  rowIndex, colIndex, QModelIndex()),
                  COLOR_LIST[color], Qt::BackgroundRole);
            }
         }

         // If the row is shown, add this rows data to the count.
         for (int partIndex = 0; partIndex < PART_COUNT; ++partIndex)
         {
            PartWidgetItem* partWidget = dynamic_cast<PartWidgetItem*>(mUI.gearMemberTable->cellWidget(rowIndex, GEAR_COLUMN_PART_START_INDEX + partIndex));
            if (partWidget)
            {
               int grade = partWidget->GetGrade(); //- 1 + 1; // -1 to fix the index in case 'None' is selected.  +1 to start at the next grade higher.

               // Increment each grade count that is at or above the selected grade
               // for this part.  If it is a weapon part, only count it if the
               // weapon type has been defined.
               if (!partWidget->HasWeapon() || partWidget->GetWeaponType() > 0)
               {
                  for (; grade < GRADE_COUNT; ++grade)
                  {
                     needCount[partIndex][grade]++;
                  }
               }
            }
         }
      }
   }

   // Show need counts for each grade and part.
   for (int partIndex = 0; partIndex < PART_COUNT; ++partIndex)
   {
      for (int gradeIndex = 0; gradeIndex < GRADE_COUNT; ++gradeIndex)
      {
         int count = needCount[partIndex][gradeIndex];

         QTableWidgetItem* item = mUI.gearSummaryTable->item(partIndex, gradeIndex);
         if (!item)
         {
            item = new QTableWidgetItem();
            item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
            item->setTextAlignment(Qt::AlignCenter);
            mUI.gearSummaryTable->setItem(partIndex, gradeIndex, item);
         }

         if (item)
         {
            item->setText(QString::number(count));
         }
      }
   }

   mUI.gearMemberTable->setSortingEnabled(true);
   mUI.gearMemberTable->blockSignals(false);
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::GearRememberTableScroll()
{
   mGearTableHScroll = mUI.gearMemberTable->horizontalScrollBar()->sliderPosition();
   mGearTableVScroll = mUI.gearMemberTable->verticalScrollBar()->sliderPosition();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::GearRestoreTableScroll()
{
   QTimer::singleShot(0, this, SLOT(on_gearTableScroll_timeout()));
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::GearResetTableScroll(bool scrollToBottom)
{
   mUI.gearMemberTable->horizontalScrollBar()->triggerAction(QScrollBar::SliderToMinimum);

   if (scrollToBottom)
   {
      mUI.gearMemberTable->verticalScrollBar()->triggerAction(QScrollBar::SliderToMaximum);
   }
   else
   {
      mUI.gearMemberTable->verticalScrollBar()->triggerAction(QScrollBar::SliderToMinimum);
   }
}

////////////////////////////////////////////////////////////////////////////////
