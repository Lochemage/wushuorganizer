/*
 * WushuOrganizer
 * Copyright (C) 2013 Jeff P. Houde (Lochemage)
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include <MainWindow.h>

#include <QtGui>
#include <QVariant>
#include <QFile>
#include <QTextStream>
#include <PartWidgetItem.h>
#include <ItemDataWidget.h>


////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesShowPeopleButton_clicked(bool)
{
   mUI.salesLeftStack->setCurrentIndex(SALES_STACK_PEOPLE);
   mUI.salesRightStack->setCurrentIndex(SALES_STACK_PEOPLE);
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesShowItemsButton_clicked(bool)
{
   mUI.salesLeftStack->setCurrentIndex(SALES_STACK_ITEMS);
   mUI.salesRightStack->setCurrentIndex(SALES_STACK_ITEMS);
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemAddButton_clicked(bool)
{
   QSettings settings(COMPANY_NAME, APPLICATION_NAME);
   settings.beginGroup("sales");
      settings.beginGroup("defaultItemTableFields");
         ItemData data;
         data.name = "-----------";
         data.type = settings.value("type", ITEM_TYPE_LIST["Key"][0]).toString();
         data.subType = settings.value("subType", ITEM_TYPE_LIST[data.type][0]).toString();
         data.grade = settings.value("grade", GRADE_LIST[0]).toString();
      settings.endGroup();
   settings.endGroup();

   // Add a new item into the table.
   SalesAddItemRow(data);
   SalesRefreshItems(false);
   SalesResetItemTableScroll();

   mUI.salesItemNameEdit->setFocus();
   mUI.salesItemNameEdit->selectAll();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemTable_itemSelectionChanged()
{
   SalesRememberItemTableScroll();
   SalesRefreshItems(false);
   SalesRestoreItemTableScroll();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemTable_cellChanged(int row, int col)
{
   QTableWidgetItem* item = mUI.salesItemTable->item(row, col);
   if (!item)
   {
      return;
   }

   ItemDataWidget* dataWidget = dynamic_cast<ItemDataWidget*>(mUI.salesItemTable->item(row, SALES_COLUMN_DATA));
   if (!dataWidget)
   {
      return;
   }

   ItemData& data = dataWidget->GetData();

   switch (col)
   {
   case SALES_COLUMN_NAME:
      {
         data.name = item->text();

         QTableWidgetItem* headerNameWidget = mUI.salesItemTable->verticalHeaderItem(row);
         if (headerNameWidget)
         {
            headerNameWidget->setText(data.name);
         }
         SetModified(true);
      }
      break;
   }

   SalesRememberItemTableScroll();
   SalesRefreshItems(false);
   SalesRestoreItemTableScroll();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemTable_typeComboChanged(int)
{
   int rowCount = mUI.salesItemTable->rowCount();
   for (int row = 0; row < rowCount; ++row)
   {
      NoScrollComboBox* typeCombo = dynamic_cast<NoScrollComboBox*>(mUI.salesItemTable->cellWidget(row, SALES_COLUMN_TYPE));
      if (typeCombo && typeCombo == sender())
      {
         ItemDataWidget* dataWidget = dynamic_cast<ItemDataWidget*>(mUI.salesItemTable->item(row, SALES_COLUMN_DATA));
         if (dataWidget)
         {
            ItemData& data = dataWidget->GetData();
            data.type = typeCombo->currentText();

            NoScrollComboBox* subTypeCombo = dynamic_cast<NoScrollComboBox*>(mUI.salesItemTable->cellWidget(row, SALES_COLUMN_SUB_TYPE));
            if (subTypeCombo)
            {
               subTypeCombo->clear();
               subTypeCombo->addItems(ITEM_TYPE_LIST[data.type]);
               subTypeCombo->setCurrentIndex(0);
            }

            SalesRememberItemTableScroll();
            SalesRefreshItems(false);
            SalesRestoreItemTableScroll();
            SetModified(true);
         }
         return;
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemTable_subTypeComboChanged(int)
{
   int rowCount = mUI.salesItemTable->rowCount();
   for (int row = 0; row < rowCount; ++row)
   {
      NoScrollComboBox* subTypeCombo = dynamic_cast<NoScrollComboBox*>(mUI.salesItemTable->cellWidget(row, SALES_COLUMN_SUB_TYPE));
      if (subTypeCombo && subTypeCombo == sender())
      {
         ItemDataWidget* dataWidget = dynamic_cast<ItemDataWidget*>(mUI.salesItemTable->item(row, SALES_COLUMN_DATA));
         if (dataWidget)
         {
            ItemData& data = dataWidget->GetData();
            data.subType = subTypeCombo->currentText();

            SalesRememberItemTableScroll();
            SalesRefreshItems(false);
            SalesRestoreItemTableScroll();
            SetModified(true);
         }
         return;
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemTable_gradeComboChanged(int)
{
   int rowCount = mUI.salesItemTable->rowCount();
   for (int row = 0; row < rowCount; ++row)
   {
      PartWidgetItem* gradeCombo = dynamic_cast<PartWidgetItem*>(mUI.salesItemTable->cellWidget(row, SALES_COLUMN_GRADE));
      if (gradeCombo && gradeCombo == sender())
      {
         ItemDataWidget* dataWidget = dynamic_cast<ItemDataWidget*>(mUI.salesItemTable->item(row, SALES_COLUMN_DATA));
         if (dataWidget)
         {
            ItemData& data = dataWidget->GetData();
            int grade = gradeCombo->GetGrade() - 1;
            if (grade > -1 && grade < GRADE_COUNT)
            {
               data.grade = GRADE_LIST[grade];
            }

            SalesRememberItemTableScroll();
            SalesRefreshItems(false);
            SalesRestoreItemTableScroll();
            SetModified(true);
         }
         return;
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemTable_deleteButtonClicked(bool)
{
   int rowCount = mUI.salesItemTable->rowCount();
   for (int rowIndex = 0; rowIndex < rowCount; ++rowIndex)
   {
      QPushButton* deleteButton = dynamic_cast<QPushButton*>(mUI.salesItemTable->cellWidget(rowIndex, SALES_COLUMN_DELETE));
      if (deleteButton && deleteButton == sender())
      {
         QTableWidgetItem* nameItem = mUI.salesItemTable->item(rowIndex, SALES_COLUMN_NAME);
         if (nameItem)
         {
            if (QMessageBox::question(this,
               "Delete item",
               "Are you sure you wish to delete item: " + nameItem->text() + "?",
               QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes)
            {
               ItemDataWidget* dataWidget = dynamic_cast<ItemDataWidget*>(mUI.salesItemTable->item(rowIndex, SALES_COLUMN_DATA));
               if (dataWidget)
               {
                  ItemData& data = dataWidget->GetData();
                  data.ClearCustomers();
               }

               SalesRememberItemTableScroll();
               mUI.salesItemTable->removeRow(rowIndex);
               SalesRefreshItems(false);
               SalesRestoreItemTableScroll();
               SetModified(true);
            }
         }
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemNameEdit_editingFinished()
{
   int row = mUI.salesItemTable->currentRow();
   if (row > -1 && row < mUI.salesItemTable->rowCount())
   {
      ItemDataWidget* dataItem = dynamic_cast<ItemDataWidget*>(mUI.salesItemTable->item(row, SALES_COLUMN_DATA));
      if (dataItem)
      {
         ItemData& data = dataItem->GetData();
         data.name = mUI.salesItemNameEdit->text();
         SetModified(true);

         QTableWidgetItem* item = mUI.salesItemTable->item(row, SALES_COLUMN_NAME);
         if (item)
         {
            item->setText(data.name);
         }

         QTableWidgetItem* headerItem = mUI.salesItemTable->verticalHeaderItem(row);
         if (headerItem)
         {
            headerItem->setText(data.name);
         }
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemTypeCombo_currentIndexChanged(int)
{
   int row = mUI.salesItemTable->currentRow();
   if (row > -1 && row < mUI.salesItemTable->rowCount())
   {
      ItemDataWidget* dataItem = dynamic_cast<ItemDataWidget*>(mUI.salesItemTable->item(row, SALES_COLUMN_DATA));
      if (dataItem)
      {
         ItemData& data = dataItem->GetData();
         data.type = mUI.salesItemTypeCombo->currentText();

         mUI.salesItemSubTypeCombo->clear();
         mUI.salesItemSubTypeCombo->addItems(ITEM_TYPE_LIST[data.type]);
         mUI.salesItemSubTypeCombo->setCurrentIndex(0);

         QComboBox* typeCombo = dynamic_cast<QComboBox*>(mUI.salesItemTable->cellWidget(row, SALES_COLUMN_TYPE));
         if (typeCombo)
         {
            typeCombo->setCurrentIndex(typeCombo->findText(data.type));
            SetModified(true);
         }
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemSubTypeCombo_currentIndexChanged(int)
{
   int row = mUI.salesItemTable->currentRow();
   if (row > -1 && row < mUI.salesItemTable->rowCount())
   {
      ItemDataWidget* dataItem = dynamic_cast<ItemDataWidget*>(mUI.salesItemTable->item(row, SALES_COLUMN_DATA));
      if (dataItem)
      {
         ItemData& data = dataItem->GetData();
         data.subType = mUI.salesItemSubTypeCombo->currentText();

         QComboBox* subTypeCombo = dynamic_cast<QComboBox*>(mUI.salesItemTable->cellWidget(row, SALES_COLUMN_SUB_TYPE));
         if (subTypeCombo)
         {
            subTypeCombo->setCurrentIndex(subTypeCombo->findText(data.subType));
            SetModified(true);
         }
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemGradeCombo_currentIndexChanged(int)
{
   int row = mUI.salesItemTable->currentRow();
   if (row > -1 && row < mUI.salesItemTable->rowCount())
   {
      ItemDataWidget* dataItem = dynamic_cast<ItemDataWidget*>(mUI.salesItemTable->item(row, SALES_COLUMN_DATA));
      if (dataItem)
      {
         ItemData& data = dataItem->GetData();
         data.grade = mUI.salesItemGradeCombo->currentText();

         PartWidgetItem* item = dynamic_cast<PartWidgetItem*>(mUI.salesItemTable->item(row, SALES_COLUMN_GRADE));
         if (item)
         {
            item->SetGrade(data.grade);
            SetModified(true);
         }
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemStackSizeSpin_valueChanged(int)
{
   int row = mUI.salesItemTable->currentRow();
   if (row > -1 && row < mUI.salesItemTable->rowCount())
   {
      ItemDataWidget* dataItem = dynamic_cast<ItemDataWidget*>(mUI.salesItemTable->item(row, SALES_COLUMN_DATA));
      if (dataItem)
      {
         ItemData& data = dataItem->GetData();
         data.stackSize = mUI.salesItemStackSizeSpin->value();
         SetModified(true);
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemMinSellPrice_valueChanged()
{
   int row = mUI.salesItemTable->currentRow();
   if (row > -1 && row < mUI.salesItemTable->rowCount())
   {
      ItemDataWidget* dataItem = dynamic_cast<ItemDataWidget*>(mUI.salesItemTable->item(row, SALES_COLUMN_DATA));
      if (dataItem)
      {
         ItemData& data = dataItem->GetData();
         data.sellMinPrice = mUI.salesItemMinSellPrice->GetPrice();
         SetModified(true);

         QLabel* sellPriceLabel = dynamic_cast<QLabel*>(mUI.salesItemTable->cellWidget(row, SALES_COLUMN_SELL_PRICE));
         if (sellPriceLabel)
         {
            sellPriceLabel->setText(MinMaxPriceWidgetItem::ToString(data.sellMinPrice, data.sellMaxPrice));
         }

         MinMaxPriceWidgetItem* sellPriceWidget = dynamic_cast<MinMaxPriceWidgetItem*>(mUI.salesItemTable->item(row, SALES_COLUMN_SELL_PRICE));
         if (sellPriceWidget)
         {
            sellPriceWidget->SetMinPrice(data.sellMinPrice);
            sellPriceWidget->SetMaxPrice(data.sellMaxPrice);
         }
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemMaxSellPrice_valueChanged()
{
   int row = mUI.salesItemTable->currentRow();
   if (row > -1 && row < mUI.salesItemTable->rowCount())
   {
      ItemDataWidget* dataItem = dynamic_cast<ItemDataWidget*>(mUI.salesItemTable->item(row, SALES_COLUMN_DATA));
      if (dataItem)
      {
         ItemData& data = dataItem->GetData();
         data.sellMaxPrice = mUI.salesItemMaxSellPrice->GetPrice();
         SetModified(true);

         QLabel* sellPriceLabel = dynamic_cast<QLabel*>(mUI.salesItemTable->cellWidget(row, SALES_COLUMN_SELL_PRICE));
         if (sellPriceLabel)
         {
            sellPriceLabel->setText(MinMaxPriceWidgetItem::ToString(data.sellMinPrice, data.sellMaxPrice));
         }

         MinMaxPriceWidgetItem* sellPriceWidget = dynamic_cast<MinMaxPriceWidgetItem*>(mUI.salesItemTable->item(row, SALES_COLUMN_SELL_PRICE));
         if (sellPriceWidget)
         {
            sellPriceWidget->SetMinPrice(data.sellMinPrice);
            sellPriceWidget->SetMaxPrice(data.sellMaxPrice);
         }
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemMinBuyPrice_valueChanged()
{
   int row = mUI.salesItemTable->currentRow();
   if (row > -1 && row < mUI.salesItemTable->rowCount())
   {
      ItemDataWidget* dataItem = dynamic_cast<ItemDataWidget*>(mUI.salesItemTable->item(row, SALES_COLUMN_DATA));
      if (dataItem)
      {
         ItemData& data = dataItem->GetData();
         data.buyMinPrice = mUI.salesItemMinBuyPrice->GetPrice();
         SetModified(true);

         QLabel* buyPriceLabel = dynamic_cast<QLabel*>(mUI.salesItemTable->cellWidget(row, SALES_COLUMN_BUY_PRICE));
         if (buyPriceLabel)
         {
            buyPriceLabel->setText(MinMaxPriceWidgetItem::ToString(data.buyMinPrice, data.buyMaxPrice));
         }

         MinMaxPriceWidgetItem* buyPriceWidget = dynamic_cast<MinMaxPriceWidgetItem*>(mUI.salesItemTable->item(row, SALES_COLUMN_BUY_PRICE));
         if (buyPriceWidget)
         {
            buyPriceWidget->SetMinPrice(data.buyMinPrice);
            buyPriceWidget->SetMaxPrice(data.buyMaxPrice);
         }
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemMaxBuyPrice_valueChanged()
{
   int row = mUI.salesItemTable->currentRow();
   if (row > -1 && row < mUI.salesItemTable->rowCount())
   {
      ItemDataWidget* dataItem = dynamic_cast<ItemDataWidget*>(mUI.salesItemTable->item(row, SALES_COLUMN_DATA));
      if (dataItem)
      {
         ItemData& data = dataItem->GetData();
         data.buyMaxPrice = mUI.salesItemMaxBuyPrice->GetPrice();
         SetModified(true);

         QLabel* buyPriceLabel = dynamic_cast<QLabel*>(mUI.salesItemTable->cellWidget(row, SALES_COLUMN_BUY_PRICE));
         if (buyPriceLabel)
         {
            buyPriceLabel->setText(MinMaxPriceWidgetItem::ToString(data.buyMinPrice, data.buyMaxPrice));
         }

         MinMaxPriceWidgetItem* buyPriceWidget = dynamic_cast<MinMaxPriceWidgetItem*>(mUI.salesItemTable->item(row, SALES_COLUMN_BUY_PRICE));
         if (buyPriceWidget)
         {
            buyPriceWidget->SetMinPrice(data.buyMinPrice);
            buyPriceWidget->SetMaxPrice(data.buyMaxPrice);
         }
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemCommentEdit_textChanged()
{
   int row = mUI.salesItemTable->currentRow();
   if (row > -1 && row < mUI.salesItemTable->rowCount())
   {
      ItemDataWidget* dataItem = dynamic_cast<ItemDataWidget*>(mUI.salesItemTable->item(row, SALES_COLUMN_DATA));
      if (dataItem)
      {
         ItemData& data = dataItem->GetData();
         data.comment = mUI.salesItemCommentEdit->toPlainText();
         SetModified(true);
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemFilterSearchEdit_editingFinished()
{
   SalesRememberItemTableScroll();
   SalesRefreshItems(true);
   SalesRestoreItemTableScroll();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemFilterTypeCombo_currentIndexChanged(int)
{
   mUI.salesItemFilterSubTypeCombo->blockSignals(true);
   mUI.salesItemFilterSubTypeCombo->clear();
   mUI.salesItemFilterSubTypeCombo->addItem("None");
   QString type = mUI.salesItemFilterTypeCombo->currentText();
   if (type != "None")
   {
      mUI.salesItemFilterSubTypeCombo->addItems(ITEM_TYPE_LIST[type]);
   }
   mUI.salesItemFilterSubTypeCombo->blockSignals(false);

   SalesRememberItemTableScroll();
   SalesRefreshItems(true);
   SalesRestoreItemTableScroll();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemFilterSubTypeCombo_currentIndexChanged(int)
{
   SalesRememberItemTableScroll();
   SalesRefreshItems(true);
   SalesRestoreItemTableScroll();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemFilterGradeCombo_currentIndexChanged(int)
{
   SalesRememberItemTableScroll();
   SalesRefreshItems(true);
   SalesRestoreItemTableScroll();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemFilterClearButton_clicked(bool)
{
   mUI.salesItemFilterSearchEdit->setText("");
   mUI.salesItemFilterTypeCombo->setCurrentIndex(0);
   mUI.salesItemFilterSubTypeCombo->setCurrentIndex(0);
   mUI.salesItemFilterGradeCombo->setCurrentIndex(0);

   SalesRememberItemTableScroll();
   SalesRefreshItems(true);
   SalesRestoreItemTableScroll();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesAddCustomerButton_clicked(bool)
{
   PersonData* personData = new PersonData();

   // Add a new item into the table.
   SalesAddCustomerRow(personData);
   SalesResetItemCustomersTableScroll();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemCustomersTable_cellChanged(int row, int col)
{
   QTableWidgetItem* item = mUI.salesItemCustomersTable->item(row, col);
   if (!item)
   {
      return;
   }

   PersonDataWidget* personDataItem = dynamic_cast<PersonDataWidget*>(mUI.salesItemCustomersTable->item(row, SALES_PERSON_COLUMN_DATA));
   if (!personDataItem)
   {
      return;
   }

   PersonData* personData = personDataItem->GetData();
   if (!personData)
   {
      return;
   }

   personData->name = item->text();
   SetModified(true);
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemCustomersTable_actionIndexChanged(int)
{
   int rowCount = mUI.salesItemCustomersTable->rowCount();
   for (int rowIndex = 0; rowIndex < rowCount; ++rowIndex)
   {
      NoScrollComboBox* actionCombo = dynamic_cast<NoScrollComboBox*>(mUI.salesItemCustomersTable->cellWidget(rowIndex, SALES_PERSON_COLUMN_ACTION));
      if (actionCombo && actionCombo == sender())
      {
         PersonDataWidget* personDataItem = dynamic_cast<PersonDataWidget*>(mUI.salesItemCustomersTable->item(rowIndex, SALES_PERSON_COLUMN_DATA));
         if (personDataItem)
         {
            PersonData* personData = personDataItem->GetData();
            if (personData)
            {
               personData->action = actionCombo->currentText();
               SetModified(true);
            }
         }
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemCustomersTable_countValueChanged(int)
{
   int rowCount = mUI.salesItemCustomersTable->rowCount();
   for (int rowIndex = 0; rowIndex < rowCount; ++rowIndex)
   {
      QSpinBox* countSpin = dynamic_cast<QSpinBox*>(mUI.salesItemCustomersTable->cellWidget(rowIndex, SALES_PERSON_COLUMN_COUNT));
      if (countSpin && countSpin == sender())
      {
         PersonDataWidget* personDataItem = dynamic_cast<PersonDataWidget*>(mUI.salesItemCustomersTable->item(rowIndex, SALES_PERSON_COLUMN_DATA));
         if (personDataItem)
         {
            PersonData* personData = personDataItem->GetData();
            if (personData)
            {
               personData->count = countSpin->value();
               SetModified(true);
            }
         }
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemCustomersTable_priceValueChanged()
{
   int rowCount = mUI.salesItemCustomersTable->rowCount();
   for (int rowIndex = 0; rowIndex < rowCount; ++rowIndex)
   {
      PriceWidgetItem* priceWidget = dynamic_cast<PriceWidgetItem*>(mUI.salesItemCustomersTable->cellWidget(rowIndex, SALES_PERSON_COLUMN_PRICE));
      if (priceWidget && priceWidget == sender())
      {
         PersonDataWidget* personDataItem = dynamic_cast<PersonDataWidget*>(mUI.salesItemCustomersTable->item(rowIndex, SALES_PERSON_COLUMN_DATA));
         if (personDataItem)
         {
            PersonData* personData = personDataItem->GetData();
            if (personData)
            {
               personData->price = priceWidget->GetPrice();
               SetModified(true);
            }
         }
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemCustomersTable_deleteButtonClicked(bool)
{
   int rowCount = mUI.salesItemCustomersTable->rowCount();
   for (int rowIndex = 0; rowIndex < rowCount; ++rowIndex)
   {
      QPushButton* deleteButton = dynamic_cast<QPushButton*>(mUI.salesItemCustomersTable->cellWidget(rowIndex, SALES_PERSON_COLUMN_DELETE));
      if (deleteButton && deleteButton == sender())
      {
         QTableWidgetItem* nameItem = mUI.salesItemCustomersTable->item(rowIndex, SALES_PERSON_COLUMN_NAME);
         if (nameItem)
         {
            if (QMessageBox::question(this,
               "Delete customer",
               "Are you sure you wish to delete customer: " + nameItem->text() + "?",
               QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes)
            {
               PersonDataWidget* personDataItem = dynamic_cast<PersonDataWidget*>(mUI.salesItemCustomersTable->item(rowIndex, SALES_PERSON_COLUMN_DATA));
               if (personDataItem)
               {
                  PersonData* personData = personDataItem->GetData();
                  int row = mUI.salesItemTable->currentRow();
                  if (row > -1 && row < mUI.salesItemTable->rowCount())
                  {
                     ItemDataWidget* dataItem = dynamic_cast<ItemDataWidget*>(mUI.salesItemTable->item(row, SALES_COLUMN_DATA));
                     if (dataItem)
                     {
                        ItemData& data = dataItem->GetData();

                        for (int personIndex = 0; personIndex < data.customers.count(); ++personIndex)
                        {
                           if (personData == data.customers[personIndex])
                           {
                              data.customers.erase(data.customers.begin() + personIndex);
                              delete personData;

                              QTableWidgetItem* customersWidget = mUI.salesItemTable->item(row, SALES_COLUMN_CUSTOMERS);
                              if (customersWidget)
                              {
                                 customersWidget->setText("x" + QString::number(data.customers.count()));
                              }

                              break;
                           }
                        }
                     }
                  }
               }

               SalesRememberItemCustomersTableScroll();
               mUI.salesItemCustomersTable->removeRow(rowIndex);
               SalesRestoreItemCustomersTableScroll();
               SetModified(true);
            }
         }
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemTableScroll_timeout()
{
   mUI.salesItemTable->horizontalScrollBar()->setSliderPosition(mSalesItemTableHScroll);
   mUI.salesItemTable->verticalScrollBar()->setSliderPosition(mSalesItemTableVScroll);
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::on_salesItemCustomersTableScroll_timeout()
{
   mUI.salesItemCustomersTable->horizontalScrollBar()->setSliderPosition(mSalesItemCustomersTableHScroll);
   mUI.salesItemCustomersTable->verticalScrollBar()->setSliderPosition(mSalesItemCustomersTableVScroll);
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::SalesClearData()
{
   int rowCount = mUI.salesItemTable->rowCount();
   for (int rowIndex = 0; rowIndex < rowCount; ++rowIndex)
   {
      ItemDataWidget* dataWidget = dynamic_cast<ItemDataWidget*>(mUI.salesItemTable->item(rowIndex, SALES_COLUMN_DATA));
      if (dataWidget)
      {
         ItemData& data = dataWidget->GetData();
         data.ClearCustomers();
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
int MainWindow::SalesGetLoadingCount() const
{
   return mUI.salesItemTable->rowCount();
}

////////////////////////////////////////////////////////////////////////////////
int MainWindow::SalesLoadData(pugi::xml_node mainNode, int version, QProgressDialog* progress)
{
   progress->setLabelText("Loading Items...");

   // Clear the table first.
   mUI.salesItemTable->setRowCount(0);

   pugi::xml_node salesNode = mainNode.child(XML_SALES_ELEMENT);
   if (!salesNode)
   {
      return LOAD_ERROR_FAILED;
   }

   // Restore filter settings.
   pugi::xml_node filtersNode = salesNode.child(XML_FILTERS_ELEMENT);
   if (filtersNode)
   {
      mUI.salesItemFilterSearchEdit->setText(filtersNode.child_value(XML_TEXT_ELEMENT));
      mUI.salesItemFilterTypeCombo->setCurrentIndex(mUI.salesItemFilterTypeCombo->findText(filtersNode.child_value(XML_TYPE_ELEMENT)));
      mUI.salesItemFilterSubTypeCombo->setCurrentIndex(mUI.salesItemFilterSubTypeCombo->findText(filtersNode.child_value(XML_SUB_TYPE_ELEMENT)));
      mUI.salesItemFilterGradeCombo->setCurrentIndex(mUI.salesItemFilterGradeCombo->findText(filtersNode.child_value(XML_GRADE_ELEMENT)));
   }

   QList<int> hiddenRows;

   pugi::xml_node itemNode = salesNode.child(XML_ITEMS_ELEMENT).child(XML_ITEM_ELEMENT);
   while (itemNode)
   {
      ItemData data;
      data.name = itemNode.child_value(XML_NAME_ELEMENT);
      if (version < 4)
      {
         QString oldType = itemNode.child_value(XML_TYPE_ELEMENT);
         // Match the old type with a similar one of the new types.
         if (ITEM_TYPE_LIST["Key"].contains(oldType))
         {
            data.type = oldType;
            data.subType = ITEM_TYPE_LIST[oldType][0];
         }
         else
         {
            bool found = false;
            int keyCount = ITEM_TYPE_LIST["Key"].count();
            for (int keyIndex = 0; keyIndex < keyCount; ++keyIndex)
            {
               QString key = ITEM_TYPE_LIST["Key"][keyIndex];
               if (ITEM_TYPE_LIST[key].contains(oldType))
               {
                  data.type = key;
                  data.subType = oldType;
                  found = true;
                  break;
               }

               if (found)
               {
                  break;
               }
            }

            // Last case scenario, specific cases.
            if (!found)
            {
               if (oldType == "Martial Skill")
               {
                  data.type = "Skill";
                  data.subType = "Martial Arts";
               }
               else if (oldType == "Flying Skill")
               {
                  data.type = "Skill";
                  data.subType = "Flying";
               }
            }
         }
      }
      else
      {
         data.type = itemNode.child_value(XML_TYPE_ELEMENT);
         data.subType = itemNode.child_value(XML_SUB_TYPE_ELEMENT);
      }
      data.grade = itemNode.child_value(XML_GRADE_ELEMENT);
      data.stackSize = itemNode.child(XML_STACK_SIZE_ELEMENT).text().as_int();
      data.sellMinPrice = itemNode.child(XML_SELL_PRICE_ELEMENT).child(XML_MIN_ELEMENT).text().as_int();
      data.sellMaxPrice = itemNode.child(XML_SELL_PRICE_ELEMENT).child(XML_MAX_ELEMENT).text().as_int();
      data.buyMinPrice = itemNode.child(XML_BUY_PRICE_ELEMENT).child(XML_MIN_ELEMENT).text().as_int();
      data.buyMaxPrice = itemNode.child(XML_BUY_PRICE_ELEMENT).child(XML_MAX_ELEMENT).text().as_int();
      data.comment = itemNode.child_value(XML_COMMENT_ELEMENT);
      pugi::xml_node customersNode = itemNode.child(XML_CUSTOMERS_ELEMENT);
      pugi::xml_node customerNode = customersNode.child(XML_CUSTOMER_ELEMENT);
      while (customerNode)
      {
         PersonData* personData = new PersonData();
         personData->action = customerNode.child_value(XML_ACTION_ELEMENT);
         personData->name = customerNode.child_value(XML_NAME_ELEMENT);
         personData->count = customerNode.child(XML_COUNT_ELEMENT).text().as_int();
         personData->price = customerNode.child(XML_PRICE_ELEMENT).text().as_int();
         data.customers.push_back(personData);
         customerNode = customerNode.next_sibling(XML_CUSTOMER_ELEMENT);
      }

      SalesAddItemRow(data);

      progress->setValue(progress->value() + 1);
      QApplication::processEvents();
      if (progress->wasCanceled())
      {
         return LOAD_ERROR_CANCELLED;
      }

      itemNode = itemNode.next_sibling(XML_ITEM_ELEMENT);
   }

   SalesRefreshItems(false);
   SalesResetItemTableScroll(false);
   return LOAD_ERROR_NONE;
}

////////////////////////////////////////////////////////////////////////////////
bool MainWindow::SalesSaveData(pugi::xml_node mainNode) const
{
   pugi::xml_node itemsNode = mainNode.append_child(XML_SALES_ELEMENT).append_child(XML_ITEMS_ELEMENT);

   // Store filter settings.
   pugi::xml_node filtersNode = itemsNode.append_child(XML_FILTERS_ELEMENT);
   QString filterText = mUI.salesItemFilterSearchEdit->text();
   QString filterType = mUI.salesItemFilterTypeCombo->currentText();
   QString filterSubType = mUI.salesItemFilterSubTypeCombo->currentText();
   QString filterGrade = mUI.salesItemFilterGradeCombo->currentText();
   filtersNode.append_child(XML_TEXT_ELEMENT).append_child(pugi::node_pcdata).set_value(filterText.toStdString().c_str());
   filtersNode.append_child(XML_TYPE_ELEMENT).append_child(pugi::node_pcdata).set_value(filterType.toStdString().c_str());
   filtersNode.append_child(XML_SUB_TYPE_ELEMENT).append_child(pugi::node_pcdata).set_value(filterSubType.toStdString().c_str());
   filtersNode.append_child(XML_GRADE_ELEMENT).append_child(pugi::node_pcdata).set_value(filterGrade.toStdString().c_str());
   
   int rowCount = mUI.salesItemTable->rowCount();
   for (int row = 0; row < rowCount; ++row)
   {
      ItemDataWidget* dataWidget = dynamic_cast<ItemDataWidget*>(mUI.salesItemTable->item(row, SALES_COLUMN_DATA));
      if (!dataWidget)
      {
         continue;
      }

      ItemData& data = dataWidget->GetData();
      pugi::xml_node itemNode = itemsNode.append_child(XML_ITEM_ELEMENT);
      itemNode.append_child(XML_NAME_ELEMENT).append_child(pugi::node_pcdata).set_value(data.name.toStdString().c_str());
      itemNode.append_child(XML_TYPE_ELEMENT).append_child(pugi::node_pcdata).set_value(data.type.toStdString().c_str());
      itemNode.append_child(XML_SUB_TYPE_ELEMENT).append_child(pugi::node_pcdata).set_value(data.subType.toStdString().c_str());
      itemNode.append_child(XML_GRADE_ELEMENT).append_child(pugi::node_pcdata).set_value(data.grade.toStdString().c_str());
      itemNode.append_child(XML_STACK_SIZE_ELEMENT).append_child(pugi::node_pcdata).set_value(QString::number(data.stackSize).toStdString().c_str());
      pugi::xml_node sellNode = itemNode.append_child(XML_SELL_PRICE_ELEMENT);
      sellNode.append_child(XML_MIN_ELEMENT).append_child(pugi::node_pcdata).set_value(QString::number(data.sellMinPrice).toStdString().c_str());
      sellNode.append_child(XML_MAX_ELEMENT).append_child(pugi::node_pcdata).set_value(QString::number(data.sellMaxPrice).toStdString().c_str());
      pugi::xml_node buyNode = itemNode.append_child(XML_BUY_PRICE_ELEMENT);
      buyNode.append_child(XML_MIN_ELEMENT).append_child(pugi::node_pcdata).set_value(QString::number(data.buyMinPrice).toStdString().c_str());
      buyNode.append_child(XML_MAX_ELEMENT).append_child(pugi::node_pcdata).set_value(QString::number(data.buyMaxPrice).toStdString().c_str());
      itemNode.append_child(XML_COMMENT_ELEMENT).append_child(pugi::node_pcdata).set_value(data.comment.toStdString().c_str());
      pugi::xml_node customersNode = itemNode.append_child(XML_CUSTOMERS_ELEMENT);
      for (int index = 0; index < data.customers.count(); ++index)
      {
         PersonData* personData = data.customers[index];
         pugi::xml_node customerNode = customersNode.append_child(XML_CUSTOMER_ELEMENT);
         customerNode.append_child(XML_ACTION_ELEMENT).append_child(pugi::node_pcdata).set_value(personData->action.toStdString().c_str());
         customerNode.append_child(XML_NAME_ELEMENT).append_child(pugi::node_pcdata).set_value(personData->name.toStdString().c_str());
         customerNode.append_child(XML_COUNT_ELEMENT).append_child(pugi::node_pcdata).set_value(QString::number(personData->count).toStdString().c_str());
         customerNode.append_child(XML_PRICE_ELEMENT).append_child(pugi::node_pcdata).set_value(QString::number(personData->price).toStdString().c_str());
      }
   }
   return true;
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::SetupUISales()
{
   // Item table.
   QStringList labels;
   labels.push_back("Name");
   labels.push_back("Type");
   labels.push_back("Sub Type");
   labels.push_back("Grade");
   labels.push_back("Price Range");
   labels.push_back("Buy Price");
   labels.push_back("Customers");
   labels.push_back("Delete");
   labels.push_back("Data");
   mUI.salesItemTable->setColumnCount(labels.count());
   mUI.salesItemTable->setHorizontalHeaderLabels(labels);
   mUI.salesItemTable->setSortingEnabled(true);
   mUI.salesItemTable->verticalHeader()->hide();

   mUI.salesItemTable->setColumnWidth(SALES_COLUMN_NAME, 200);
   mUI.salesItemTable->setColumnWidth(SALES_COLUMN_TYPE, 100);
   mUI.salesItemTable->setColumnWidth(SALES_COLUMN_SUB_TYPE, 100);
   mUI.salesItemTable->setColumnWidth(SALES_COLUMN_GRADE, 40);
   mUI.salesItemTable->setColumnWidth(SALES_COLUMN_SELL_PRICE, 100);
   mUI.salesItemTable->setColumnWidth(SALES_COLUMN_BUY_PRICE, 100);
   mUI.salesItemTable->setColumnWidth(SALES_COLUMN_CUSTOMERS, 80);
   mUI.salesItemTable->setColumnWidth(SALES_COLUMN_DELETE, 50);
   mUI.salesItemTable->setColumnHidden(SALES_COLUMN_BUY_PRICE, true);
   mUI.salesItemTable->setColumnHidden(SALES_COLUMN_DATA, true);

   // Filters
   mUI.salesItemFilterTypeCombo->addItem("None");
   mUI.salesItemFilterTypeCombo->addItems(ITEM_TYPE_LIST["Key"]);

   // Customer table.
   labels.clear();
   labels.push_back("Action");
   labels.push_back("Name");
   labels.push_back("Count");
   labels.push_back("Price");
   labels.push_back("Delete");
   labels.push_back("Data");
   mUI.salesItemCustomersTable->setColumnCount(labels.count());
   mUI.salesItemCustomersTable->setHorizontalHeaderLabels(labels);
   mUI.salesItemCustomersTable->setSortingEnabled(true);
   mUI.salesItemCustomersTable->verticalHeader()->hide();
   mUI.salesItemCustomersTable->setColumnWidth(SALES_PERSON_COLUMN_ACTION, 80);
   mUI.salesItemCustomersTable->setColumnWidth(SALES_PERSON_COLUMN_NAME, 200);
   mUI.salesItemCustomersTable->setColumnWidth(SALES_PERSON_COLUMN_COUNT, 80);
   mUI.salesItemCustomersTable->setColumnWidth(SALES_PERSON_COLUMN_PRICE, 150);
   mUI.salesItemCustomersTable->setColumnWidth(SALES_PERSON_COLUMN_DELETE, 50);
   mUI.salesItemCustomersTable->setColumnHidden(SALES_PERSON_COLUMN_DATA, true);

   // Default splitter sizes.
   QList<int> sizes;
   sizes.push_back((width() / 5) * 2);
   sizes.push_back((width() / 5) * 3);
   mUI.salesMainSplitter->setSizes(sizes);

   sizes[0] = ((width() / 5) * 3) / 2;
   sizes[1] = sizes[0];
   mUI.salesItemSplitter->setSizes(sizes);

   mUI.salesItemTypeCombo->addItems(ITEM_TYPE_LIST["Key"]);
   mUI.salesItemTypeCombo->setCurrentIndex(0);

   mUI.salesItemSubTypeCombo->addItems(ITEM_TYPE_LIST[ITEM_TYPE_LIST["Key"][0]]);
   mUI.salesItemSubTypeCombo->setCurrentIndex(0);

   // Disable item pane.
   mUI.salesItemInfoGroup->setDisabled(true);
   mUI.salesCustomersGroup->setDisabled(true);

   connect(mUI.salesItemMinSellPrice, SIGNAL(valueChanged()), this, SLOT(on_salesItemMinSellPrice_valueChanged()));
   connect(mUI.salesItemMaxSellPrice, SIGNAL(valueChanged()), this, SLOT(on_salesItemMaxSellPrice_valueChanged()));
   connect(mUI.salesItemMinBuyPrice, SIGNAL(valueChanged()), this, SLOT(on_salesItemMinBuyPrice_valueChanged()));
   connect(mUI.salesItemMaxBuyPrice, SIGNAL(valueChanged()), this, SLOT(on_salesItemMaxBuyPrice_valueChanged()));
}

////////////////////////////////////////////////////////////////////////////////
int MainWindow::SalesAddItemRow(const ItemData& data)
{
   mUI.salesItemTable->blockSignals(true);

   mUI.salesItemTable->sortItems(SALES_COLUMN_DELETE, Qt::AscendingOrder);
   mUI.salesItemTable->setSortingEnabled(false);
   int row = mUI.salesItemTable->rowCount();
   mUI.salesItemTable->insertRow(row);

   QTableWidgetItem* nameWidget = new QTableWidgetItem(data.name);
   mUI.salesItemTable->setItem(row, SALES_COLUMN_NAME, nameWidget);

   QTableWidgetItem* headerNameWidget = new QTableWidgetItem(data.name);
   mUI.salesItemTable->setVerticalHeaderItem(row, headerNameWidget);

   NoScrollComboBox* typeCombo = new NoScrollComboBox();
   QTableWidgetItem* sortItem = new ComboSortItem(typeCombo);
   typeCombo->addItems(ITEM_TYPE_LIST["Key"]);
   typeCombo->setCurrentIndex(typeCombo->findText(data.type));
   mUI.salesItemTable->setItem(row, SALES_COLUMN_TYPE, sortItem);
   mUI.salesItemTable->setCellWidget(row, SALES_COLUMN_TYPE, typeCombo);
   connect(typeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(on_salesItemTable_typeComboChanged(int)));

   NoScrollComboBox* subTypeCombo = new NoScrollComboBox();
   sortItem = new ComboSortItem(subTypeCombo);
   subTypeCombo->addItems(ITEM_TYPE_LIST[data.type]);
   subTypeCombo->setCurrentIndex(subTypeCombo->findText(data.subType));
   mUI.salesItemTable->setItem(row, SALES_COLUMN_SUB_TYPE, sortItem);
   mUI.salesItemTable->setCellWidget(row, SALES_COLUMN_SUB_TYPE, subTypeCombo);
   connect(subTypeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(on_salesItemTable_subTypeComboChanged(int)));

   PartWidgetItem* partWidget = new PartWidgetItem();
   partWidget->SetGrade(data.grade);
   mUI.salesItemTable->setItem(row, SALES_COLUMN_GRADE, partWidget);
   mUI.salesItemTable->setCellWidget(row, SALES_COLUMN_GRADE, partWidget);
   connect(partWidget, SIGNAL(stateChanged(int)), this, SLOT(on_salesItemTable_gradeComboChanged(int)));

   QLabel* sellPriceLabel = new QLabel(MinMaxPriceWidgetItem::ToString(data.sellMinPrice, data.sellMaxPrice));
   MinMaxPriceWidgetItem* sellPriceWidget = new MinMaxPriceWidgetItem();
   sellPriceWidget->SetMinPrice(data.sellMinPrice);
   sellPriceWidget->SetMaxPrice(data.sellMaxPrice);
   sellPriceLabel->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
   mUI.salesItemTable->setItem(row, SALES_COLUMN_SELL_PRICE, sellPriceWidget);
   mUI.salesItemTable->setCellWidget(row, SALES_COLUMN_SELL_PRICE, sellPriceLabel);

   QLabel* buyPriceLabel = new QLabel(MinMaxPriceWidgetItem::ToString(data.buyMinPrice, data.buyMaxPrice));
   MinMaxPriceWidgetItem* buyPriceWidget = new MinMaxPriceWidgetItem();
   buyPriceWidget->SetMinPrice(data.buyMinPrice);
   buyPriceWidget->SetMaxPrice(data.buyMaxPrice);
   buyPriceLabel->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
   mUI.salesItemTable->setItem(row, SALES_COLUMN_BUY_PRICE, buyPriceWidget);
   mUI.salesItemTable->setCellWidget(row, SALES_COLUMN_BUY_PRICE, buyPriceLabel);

   QTableWidgetItem* customersWidget = new QTableWidgetItem("x" + QString::number(data.customers.count()));
   customersWidget->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
   customersWidget->setTextAlignment(Qt::AlignCenter);
   mUI.salesItemTable->setItem(row, SALES_COLUMN_CUSTOMERS, customersWidget);

   QPushButton* deleteButton = new QPushButton("Delete");
   QTableWidgetItem* indexSortItem = new QTableWidgetItem();
   mUI.salesItemTable->setItem(row, SALES_COLUMN_DELETE, indexSortItem); 
   mUI.salesItemTable->setCellWidget(row, SALES_COLUMN_DELETE, deleteButton);
   connect(deleteButton, SIGNAL(clicked(bool)), this, SLOT(on_salesItemTable_deleteButtonClicked(bool)));

   ItemDataWidget* dataWidget = new ItemDataWidget();
   dataWidget->SetData(data);
   mUI.salesItemTable->setItem(row, SALES_COLUMN_DATA, dataWidget);

   mUI.salesItemTable->selectRow(row);
   mUI.salesItemTable->setSortingEnabled(true);

   SetModified(true);

   mUI.salesItemTable->blockSignals(false);
   return row;
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::SalesRefreshItems(bool applyFilters)
{
   int selectedRow = mUI.salesItemTable->currentRow();

   if (applyFilters)
   {
      QString filterSearch = mUI.salesItemFilterSearchEdit->text();
      int filterType       = mUI.salesItemFilterTypeCombo->currentIndex() - 1;
      int filterSubType    = mUI.salesItemFilterSubTypeCombo->currentIndex() - 1;
      int filterGrade      = mUI.salesItemFilterGradeCombo->currentIndex() - 1;

      int rowCount = mUI.salesItemTable->rowCount();
      for (int rowIndex = 0; rowIndex < rowCount; ++rowIndex)
      {
         ItemDataWidget* item = dynamic_cast<ItemDataWidget*>(mUI.salesItemTable->item(rowIndex, SALES_COLUMN_DATA));
         if (item)
         {
            const ItemData& data = item->GetData();

            bool hideRow = false;
            if (!filterSearch.isEmpty())
            {
               if (!data.name.contains(filterSearch, Qt::CaseInsensitive))
               {
                  hideRow = true;
               }
            }

            if (filterType > -1)
            {
               if (data.type != ITEM_TYPE_LIST["Key"][filterType])
               {
                  hideRow = true;
               }
            }

            if (filterSubType > -1)
            {
               if (filterSubType < ITEM_TYPE_LIST[data.type].count())
               {
                  if (data.subType != ITEM_TYPE_LIST[data.type][filterSubType])
                  {
                     hideRow = true;
                  }
               }
            }

            if (filterGrade > -1)
            {
               if (data.grade != GRADE_LIST[filterGrade])
               {
                  hideRow = true;
               }
            }

            mUI.salesItemTable->setRowHidden(rowIndex, hideRow);
         }
      }
   }

   // Auto select the last visible row.
   if (selectedRow > -1 && !mUI.salesItemTable->isRowHidden(selectedRow))
   {
      mUI.salesItemTable->selectRow(selectedRow);
   }
   else
   {
      int rowCount = mUI.salesItemTable->rowCount();
      for (int rowIndex = 0; rowIndex < rowCount; ++rowIndex)
      {
         if (!mUI.salesItemTable->isRowHidden(rowIndex))
         {
            selectedRow = rowIndex;
         }
      }

      if (selectedRow > -1)
      {
         mUI.salesItemTable->selectRow(selectedRow);
      }
   }

   // Disable item pane.
   mUI.salesItemInfoGroup->setDisabled(true);
   mUI.salesCustomersGroup->setDisabled(true);
   if (selectedRow > -1 && selectedRow < mUI.salesItemTable->rowCount())
   {
      ItemDataWidget* item = dynamic_cast<ItemDataWidget*>(mUI.salesItemTable->item(selectedRow, SALES_COLUMN_DATA));
      if (item)
      {
         const ItemData& data = item->GetData();

         mUI.salesItemNameEdit->blockSignals(true);
         mUI.salesItemTypeCombo->blockSignals(true);
         mUI.salesItemSubTypeCombo->blockSignals(true);
         mUI.salesItemGradeCombo->blockSignals(true);
         mUI.salesItemStackSizeSpin->blockSignals(true);
         mUI.salesItemMinSellPrice->blockSignals(true);
         mUI.salesItemMaxSellPrice->blockSignals(true);
         mUI.salesItemMinBuyPrice->blockSignals(true);
         mUI.salesItemMaxBuyPrice->blockSignals(true);
         mUI.salesItemCommentEdit->blockSignals(true);
         mUI.salesItemCustomersTable->blockSignals(true);

         // Enable item pane.
         mUI.salesItemInfoGroup->setEnabled(true);
         mUI.salesCustomersGroup->setEnabled(true);

         mUI.salesItemSubTypeCombo->clear();
         mUI.salesItemSubTypeCombo->addItems(ITEM_TYPE_LIST[data.type]);
         mUI.salesItemSubTypeCombo->setCurrentIndex(0);

         mUI.salesItemNameEdit->setText(data.name);
         mUI.salesItemTypeCombo->setCurrentIndex(mUI.salesItemTypeCombo->findText(data.type));
         mUI.salesItemSubTypeCombo->setCurrentIndex(mUI.salesItemSubTypeCombo->findText(data.subType));
         mUI.salesItemGradeCombo->setCurrentIndex(mUI.salesItemGradeCombo->findText(data.grade));
         mUI.salesItemStackSizeSpin->setValue(data.stackSize);
         mUI.salesItemMinSellPrice->SetPrice(data.sellMinPrice);
         mUI.salesItemMaxSellPrice->SetPrice(data.sellMaxPrice);
         mUI.salesItemMinBuyPrice->SetPrice(data.buyMinPrice);
         mUI.salesItemMaxBuyPrice->SetPrice(data.buyMaxPrice);
         mUI.salesItemCommentEdit->setText(data.comment);

         mUI.salesItemCustomersTable->setRowCount(0);
         for (int personIndex = 0; personIndex < data.customers.count(); ++personIndex)
         {
            PersonData* personData = data.customers[personIndex];
            if (personData)
            {
               SalesAddCustomerRow(personData, false);
            }
         }

         mUI.salesItemTable->setFocus();

         mUI.salesItemNameEdit->blockSignals(false);
         mUI.salesItemTypeCombo->blockSignals(false);
         mUI.salesItemSubTypeCombo->blockSignals(false);
         mUI.salesItemGradeCombo->blockSignals(false);
         mUI.salesItemStackSizeSpin->blockSignals(false);
         mUI.salesItemMinSellPrice->blockSignals(false);
         mUI.salesItemMaxSellPrice->blockSignals(false);
         mUI.salesItemMinBuyPrice->blockSignals(false);
         mUI.salesItemMaxBuyPrice->blockSignals(false);
         mUI.salesItemCommentEdit->blockSignals(false);
         mUI.salesItemCustomersTable->blockSignals(false);
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::SalesRememberItemTableScroll()
{
   mSalesItemTableHScroll = mUI.salesItemTable->horizontalScrollBar()->sliderPosition();
   mSalesItemTableVScroll = mUI.salesItemTable->verticalScrollBar()->sliderPosition();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::SalesRestoreItemTableScroll()
{
   QTimer::singleShot(0, this, SLOT(on_salesItemTableScroll_timeout()));
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::SalesResetItemTableScroll(bool scrollToBottom)
{
   mUI.salesItemTable->horizontalScrollBar()->triggerAction(QScrollBar::SliderToMinimum);

   if (scrollToBottom)
   {
      mUI.salesItemTable->verticalScrollBar()->triggerAction(QScrollBar::SliderToMaximum);
   }
   else
   {
      mUI.salesItemTable->verticalScrollBar()->triggerAction(QScrollBar::SliderToMinimum);
   }
}

////////////////////////////////////////////////////////////////////////////////
int MainWindow::SalesAddCustomerRow(PersonData* personData, bool addNew)
{
   mUI.salesItemCustomersTable->blockSignals(true);

   mUI.salesItemCustomersTable->sortItems(SALES_PERSON_COLUMN_DELETE, Qt::AscendingOrder);
   mUI.salesItemCustomersTable->setSortingEnabled(false);
   int row = mUI.salesItemCustomersTable->rowCount();
   mUI.salesItemCustomersTable->insertRow(row);

   NoScrollComboBox* actionCombo = new NoScrollComboBox();
   actionCombo->addItems(ACTION_LIST);
   actionCombo->setCurrentIndex(actionCombo->findText(personData->action));
   mUI.salesItemCustomersTable->setCellWidget(row, SALES_PERSON_COLUMN_ACTION, actionCombo);
   connect(actionCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(on_salesItemCustomersTable_actionIndexChanged(int)));

   QTableWidgetItem* nameWidget = new QTableWidgetItem(personData->name);
   mUI.salesItemCustomersTable->setItem(row, SALES_PERSON_COLUMN_NAME, nameWidget);

   QSpinBox* countSpin = new QSpinBox();
   countSpin->setMinimum(-1);
   countSpin->setMaximum(1000);
   countSpin->setPrefix("x");
   countSpin->setSpecialValueText("Unlimited");
   countSpin->setValue(personData->count);
   mUI.salesItemCustomersTable->setCellWidget(row, SALES_PERSON_COLUMN_COUNT, countSpin);
   connect(countSpin, SIGNAL(valueChanged(int)), this, SLOT(on_salesItemCustomersTable_countValueChanged(int)));

   PriceWidgetItem* priceWidget = new PriceWidgetItem(NULL, true);
   priceWidget->SetPrice(personData->price);
   mUI.salesItemCustomersTable->setItem(row, SALES_PERSON_COLUMN_PRICE, priceWidget);
   mUI.salesItemCustomersTable->setCellWidget(row, SALES_PERSON_COLUMN_PRICE, priceWidget);
   connect(priceWidget, SIGNAL(valueChanged()), this, SLOT(on_salesItemCustomersTable_priceValueChanged()));

   QPushButton* deleteButton = new QPushButton("Delete");
   QTableWidgetItem* indexSortItem = new QTableWidgetItem();
   mUI.salesItemCustomersTable->setItem(row, SALES_PERSON_COLUMN_DELETE, indexSortItem); 
   mUI.salesItemCustomersTable->setCellWidget(row, SALES_PERSON_COLUMN_DELETE, deleteButton);
   connect(deleteButton, SIGNAL(clicked(bool)), this, SLOT(on_salesItemCustomersTable_deleteButtonClicked(bool)));

   PersonDataWidget* dataWidget = new PersonDataWidget();
   dataWidget->SetData(personData);
   mUI.salesItemCustomersTable->setItem(row, SALES_PERSON_COLUMN_DATA, dataWidget);

   mUI.salesItemCustomersTable->selectRow(row);
   mUI.salesItemCustomersTable->setSortingEnabled(true);

   if (addNew)
   {
      int subRow = mUI.salesItemTable->currentRow();
      if (subRow > -1 && subRow < mUI.salesItemTable->rowCount())
      {
         ItemDataWidget* dataItem = dynamic_cast<ItemDataWidget*>(mUI.salesItemTable->item(subRow, SALES_COLUMN_DATA));
         if (dataItem)
         {
            ItemData& data = dataItem->GetData();
            data.customers.push_back(personData);

            QTableWidgetItem* customersWidget = mUI.salesItemTable->item(subRow, SALES_COLUMN_CUSTOMERS);
            if (customersWidget)
            {
               customersWidget->setText("x" + QString::number(data.customers.count()));
            }
         }
      }

      SetModified(true);
   }

   mUI.salesItemCustomersTable->blockSignals(false);
   return row;
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::SalesRememberItemCustomersTableScroll()
{
   mSalesItemCustomersTableHScroll = mUI.salesItemCustomersTable->horizontalScrollBar()->sliderPosition();
   mSalesItemCustomersTableVScroll = mUI.salesItemCustomersTable->verticalScrollBar()->sliderPosition();
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::SalesRestoreItemCustomersTableScroll()
{
   QTimer::singleShot(0, this, SLOT(on_salesItemCustomersTableScroll_timeout()));
}

////////////////////////////////////////////////////////////////////////////////
void MainWindow::SalesResetItemCustomersTableScroll(bool scrollToBottom)
{
   mUI.salesItemCustomersTable->horizontalScrollBar()->triggerAction(QScrollBar::SliderToMinimum);

   if (scrollToBottom)
   {
      mUI.salesItemCustomersTable->verticalScrollBar()->triggerAction(QScrollBar::SliderToMaximum);
   }
   else
   {
      mUI.salesItemCustomersTable->verticalScrollBar()->triggerAction(QScrollBar::SliderToMinimum);
   }
}

////////////////////////////////////////////////////////////////////////////////
